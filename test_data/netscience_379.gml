graph [
  Creator " "Mark Newman on Sat Jul 22 06:24:59 2006""
  node [
    id 30
    label "ALBERT, R"
  ]
  node [
    id 31
    label "ALBERT, I"
  ]
  node [
    id 32
    label "NAKARADO, G"
  ]
  node [
    id 33
    label "BARABASI, A"
  ]
  node [
    id 34
    label "JEONG, H"
  ]
  node [
    id 44
    label "ALEKSIEJUK, A"
  ]
  node [
    id 45
    label "HOLYST, J"
  ]
  node [
    id 46
    label "STAUFFER, D"
  ]
  node [
    id 47
    label "ALLARIA, E"
  ]
  node [
    id 48
    label "ARECCHI, F"
  ]
  node [
    id 49
    label "DIGARBO, A"
  ]
  node [
    id 50
    label "MEUCCI, R"
  ]
  node [
    id 51
    label "ALMAAS, E"
  ]
  node [
    id 52
    label "KOVACS, B"
  ]
  node [
    id 53
    label "VICSEK, T"
  ]
  node [
    id 54
    label "OLTVAI, Z"
  ]
  node [
    id 55
    label "KRAPIVSKY, P"
  ]
  node [
    id 56
    label "REDNER, S"
  ]
  node [
    id 57
    label "KULKARNI, R"
  ]
  node [
    id 58
    label "STROUD, D"
  ]
  node [
    id 69
    label "AMARAL, L"
  ]
  node [
    id 70
    label "SCALA, A"
  ]
  node [
    id 71
    label "BARTHELEMY, M"
  ]
  node [
    id 72
    label "STANLEY, H"
  ]
  node [
    id 77
    label "ANCELMEYERS, L"
  ]
  node [
    id 78
    label "NEWMAN, M"
  ]
  node [
    id 79
    label "MARTIN, M"
  ]
  node [
    id 80
    label "SCHRAG, S"
  ]
  node [
    id 90
    label "ANTAL, T"
  ]
  node [
    id 94
    label "ARENAS, A"
  ]
  node [
    id 95
    label "CABRALES, A"
  ]
  node [
    id 96
    label "DIAZGUILERA, A"
  ]
  node [
    id 97
    label "GUIMERA, R"
  ]
  node [
    id 98
    label "VEGAREDONDO, F"
  ]
  node [
    id 99
    label "DANON, L"
  ]
  node [
    id 100
    label "GLEISER, P"
  ]
  node [
    id 113
    label "BAIESI, M"
  ]
  node [
    id 114
    label "PACZUSKI, M"
  ]
  node [
    id 120
    label "BAK, P"
  ]
  node [
    id 121
    label "SNEPPEN, K"
  ]
  node [
    id 126
    label "BANAVAR, J"
  ]
  node [
    id 127
    label "MARITAN, A"
  ]
  node [
    id 128
    label "RINALDO, A"
  ]
  node [
    id 131
    label "BIANCONI, G"
  ]
  node [
    id 132
    label "RAVASZ, E"
  ]
  node [
    id 133
    label "NEDA, Z"
  ]
  node [
    id 134
    label "SCHUBERT, A"
  ]
  node [
    id 135
    label "BARAHONA, M"
  ]
  node [
    id 136
    label "PECORA, L"
  ]
  node [
    id 149
    label "BARRAT, A"
  ]
  node [
    id 150
    label "PASTORSATORRAS, R"
  ]
  node [
    id 151
    label "VESPIGNANI, A"
  ]
  node [
    id 152
    label "WEIGT, M"
  ]
  node [
    id 157
    label "GONDRAN, B"
  ]
  node [
    id 158
    label "GUICHARD, E"
  ]
  node [
    id 162
    label "BATTISTON, S"
  ]
  node [
    id 163
    label "CATANZARO, M"
  ]
  node [
    id 184
    label "BENNAIM, E"
  ]
  node [
    id 185
    label "FRAUENFELDER, H"
  ]
  node [
    id 186
    label "TOROCZKAI, Z"
  ]
  node [
    id 190
    label "BERLOW, E"
  ]
  node [
    id 191
    label "BERNARDES, A"
  ]
  node [
    id 192
    label "COSTA, U"
  ]
  node [
    id 193
    label "ARAUJO, A"
  ]
  node [
    id 194
    label "KERTESZ, J"
  ]
  node [
    id 203
    label "CAPOCCI, A"
  ]
  node [
    id 216
    label "BOCCALETTI, S"
  ]
  node [
    id 217
    label "BRAGARD, J"
  ]
  node [
    id 218
    label "MANCINI, H"
  ]
  node [
    id 219
    label "KURTHS, J"
  ]
  node [
    id 220
    label "VALLADARES, D"
  ]
  node [
    id 221
    label "OSIPOV, G"
  ]
  node [
    id 222
    label "ZHOU, C"
  ]
  node [
    id 223
    label "PELAEZ, A"
  ]
  node [
    id 224
    label "MAZA, D"
  ]
  node [
    id 225
    label "BOGUNA, M"
  ]
  node [
    id 233
    label "BONANNO, G"
  ]
  node [
    id 234
    label "LILLO, F"
  ]
  node [
    id 235
    label "MANTEGNA, R"
  ]
  node [
    id 251
    label "MENDOZA, C"
  ]
  node [
    id 252
    label "HENTSCHEL, H"
  ]
  node [
    id 262
    label "BRODER, A"
  ]
  node [
    id 263
    label "KUMAR, R"
  ]
  node [
    id 264
    label "MAGHOUL, F"
  ]
  node [
    id 265
    label "RAGHAVAN, P"
  ]
  node [
    id 266
    label "RAJAGOPALAN, S"
  ]
  node [
    id 267
    label "STATA, R"
  ]
  node [
    id 268
    label "TOMKINS, A"
  ]
  node [
    id 269
    label "WIENER, J"
  ]
  node [
    id 276
    label "BUCOLO, M"
  ]
  node [
    id 277
    label "FORTUNA, L"
  ]
  node [
    id 278
    label "LAROSA, M"
  ]
  node [
    id 279
    label "BUHL, J"
  ]
  node [
    id 280
    label "GAUTRAIS, J"
  ]
  node [
    id 281
    label "SOLE, R"
  ]
  node [
    id 282
    label "KUNTZ, P"
  ]
  node [
    id 283
    label "VALVERDE, S"
  ]
  node [
    id 284
    label "DENEUBOURG, J"
  ]
  node [
    id 285
    label "THERAULAZ, G"
  ]
  node [
    id 301
    label "CALDARELLI, G"
  ]
  node [
    id 302
    label "DELOSRIOS, P"
  ]
  node [
    id 303
    label "MUNOZ, M"
  ]
  node [
    id 304
    label "COCCETTI, F"
  ]
  node [
    id 305
    label "CALLAWAY, D"
  ]
  node [
    id 306
    label "HOPCROFT, J"
  ]
  node [
    id 307
    label "KLEINBERG, J"
  ]
  node [
    id 308
    label "STROGATZ, S"
  ]
  node [
    id 309
    label "WATTS, D"
  ]
  node [
    id 310
    label "CAMACHO, J"
  ]
  node [
    id 316
    label "SERVEDIO, V"
  ]
  node [
    id 317
    label "COLAIORI, F"
  ]
  node [
    id 326
    label "CARUSO, F"
  ]
  node [
    id 327
    label "LATORA, V"
  ]
  node [
    id 328
    label "RAPISARDA, A"
  ]
  node [
    id 329
    label "TADIC, B"
  ]
  node [
    id 330
    label "CASTELLANO, C"
  ]
  node [
    id 331
    label "VILONE, D"
  ]
  node [
    id 342
    label "CHATE, H"
  ]
  node [
    id 343
    label "PIKOVSKY, A"
  ]
  node [
    id 344
    label "RUDZICK, O"
  ]
  node [
    id 345
    label "CHAVEZ, M"
  ]
  node [
    id 346
    label "HWANG, D"
  ]
  node [
    id 347
    label "AMANN, A"
  ]
  node [
    id 370
    label "CLAUSET, A"
  ]
  node [
    id 371
    label "MOORE, C"
  ]
  node [
    id 375
    label "COHEN, R"
  ]
  node [
    id 376
    label "BENAVRAHAM, D"
  ]
  node [
    id 377
    label "HAVLIN, S"
  ]
  node [
    id 378
    label "EREZ, K"
  ]
  node [
    id 401
    label "COSENZA, S"
  ]
  node [
    id 402
    label "CRUCITTI, P"
  ]
  node [
    id 403
    label "FRASCA, M"
  ]
  node [
    id 404
    label "STAGNI, C"
  ]
  node [
    id 405
    label "USAI, L"
  ]
  node [
    id 416
    label "MARCHIORI, M"
  ]
  node [
    id 417
    label "PORTA, S"
  ]
  node [
    id 428
    label "DAFONTOURACOSTA, L"
  ]
  node [
    id 429
    label "DIAMBRA, L"
  ]
  node [
    id 442
    label "DEARCANGELIS, L"
  ]
  node [
    id 443
    label "HERRMANN, H"
  ]
  node [
    id 450
    label "DEFRAYSSEIX, H"
  ]
  node [
    id 460
    label "DELUCIA, M"
  ]
  node [
    id 461
    label "BOTTACCIO, M"
  ]
  node [
    id 462
    label "MONTUORI, M"
  ]
  node [
    id 463
    label "PIETRONERO, L"
  ]
  node [
    id 464
    label "DEMENEZES, M"
  ]
  node [
    id 465
    label "MOUKARZEL, C"
  ]
  node [
    id 466
    label "PENNA, T"
  ]
  node [
    id 472
    label "DEMOURA, A"
  ]
  node [
    id 473
    label "MOTTER, A"
  ]
  node [
    id 474
    label "GREBOGI, C"
  ]
  node [
    id 485
    label "DEZSO, Z"
  ]
  node [
    id 488
    label "DOBRIN, R"
  ]
  node [
    id 489
    label "BEG, Q"
  ]
  node [
    id 490
    label "DODDS, P"
  ]
  node [
    id 491
    label "MUHAMAD, R"
  ]
  node [
    id 492
    label "ROTHMAN, D"
  ]
  node [
    id 493
    label "SABEL, C"
  ]
  node [
    id 499
    label "DONETTI, L"
  ]
  node [
    id 500
    label "DOROGOVTSEV, S"
  ]
  node [
    id 501
    label "GOLTSEV, A"
  ]
  node [
    id 502
    label "MENDES, J"
  ]
  node [
    id 503
    label "SAMUKHIN, A"
  ]
  node [
    id 507
    label "DUNNE, J"
  ]
  node [
    id 508
    label "WILLIAMS, R"
  ]
  node [
    id 509
    label "MARTINEZ, N"
  ]
  node [
    id 514
    label "ECHENIQUE, P"
  ]
  node [
    id 515
    label "GOMEZGARDENES, J"
  ]
  node [
    id 516
    label "MORENO, Y"
  ]
  node [
    id 517
    label "VAZQUEZ, A"
  ]
  node [
    id 546
    label "ERGUN, G"
  ]
  node [
    id 547
    label "RODGERS, G"
  ]
  node [
    id 548
    label "ERIKSEN, K"
  ]
  node [
    id 549
    label "SIMONSEN, I"
  ]
  node [
    id 550
    label "MASLOV, S"
  ]
  node [
    id 561
    label "FARKAS, I"
  ]
  node [
    id 562
    label "DERENYI, I"
  ]
  node [
    id 574
    label "FERRERICANCHO, R"
  ]
  node [
    id 575
    label "JANSSEN, C"
  ]
  node [
    id 576
    label "KOHLER, R"
  ]
  node [
    id 585
    label "FINK, K"
  ]
  node [
    id 586
    label "JOHNSON, G"
  ]
  node [
    id 587
    label "CARROLL, T"
  ]
  node [
    id 589
    label "FLAKE, G"
  ]
  node [
    id 590
    label "LAWRENCE, S"
  ]
  node [
    id 591
    label "GILES, C"
  ]
  node [
    id 592
    label "COETZEE, F"
  ]
  node [
    id 595
    label "SPATA, A"
  ]
  node [
    id 596
    label "FORTUNATO, S"
  ]
  node [
    id 609
    label "FRONCZAK, A"
  ]
  node [
    id 610
    label "FRONCZAK, P"
  ]
  node [
    id 611
    label "JEDYNAK, M"
  ]
  node [
    id 612
    label "SIENKIEWICZ, J"
  ]
  node [
    id 638
    label "GARLASCHELLI, D"
  ]
  node [
    id 639
    label "CASTRI, M"
  ]
  node [
    id 640
    label "LOFFREDO, M"
  ]
  node [
    id 641
    label "GASTNER, M"
  ]
  node [
    id 646
    label "GIRVAN, M"
  ]
  node [
    id 652
    label "GOH, K"
  ]
  node [
    id 653
    label "GHIM, C"
  ]
  node [
    id 654
    label "KAHNG, B"
  ]
  node [
    id 655
    label "KIM, D"
  ]
  node [
    id 656
    label "LEE, D"
  ]
  node [
    id 657
    label "OH, E"
  ]
  node [
    id 674
    label "FLORIA, L"
  ]
  node [
    id 675
    label "GONZALES, M"
  ]
  node [
    id 676
    label "SOUSA, A"
  ]
  node [
    id 685
    label "GORMAN, S"
  ]
  node [
    id 692
    label "GREGOIRE, G"
  ]
  node [
    id 693
    label "GROSS, J"
  ]
  node [
    id 694
    label "KUJALA, J"
  ]
  node [
    id 695
    label "HAMALAINEN, M"
  ]
  node [
    id 696
    label "TIMMERMANN, L"
  ]
  node [
    id 697
    label "SCHNITZLER, A"
  ]
  node [
    id 698
    label "SALMELIN, R"
  ]
  node [
    id 700
    label "GUARDIOLA, X"
  ]
  node [
    id 701
    label "LLAS, M"
  ]
  node [
    id 702
    label "PEREZ, C"
  ]
  node [
    id 708
    label "GIRALT, F"
  ]
  node [
    id 709
    label "MOSSA, S"
  ]
  node [
    id 710
    label "TURTSCHI, A"
  ]
  node [
    id 715
    label "HARI, R"
  ]
  node [
    id 716
    label "ILMONIEMI, R"
  ]
  node [
    id 717
    label "KNUUTILA, J"
  ]
  node [
    id 718
    label "LOUNASMAA, O"
  ]
  node [
    id 729
    label "HEAGY, J"
  ]
  node [
    id 736
    label "HERRMANN, C"
  ]
  node [
    id 737
    label "PROVERO, P"
  ]
  node [
    id 738
    label "HONG, D"
  ]
  node [
    id 739
    label "ROUX, S"
  ]
  node [
    id 756
    label "HOLME, P"
  ]
  node [
    id 757
    label "EDLING, C"
  ]
  node [
    id 758
    label "LILJEROS, F"
  ]
  node [
    id 759
    label "GHOSHAL, G"
  ]
  node [
    id 760
    label "HUSS, M"
  ]
  node [
    id 761
    label "KIM, B"
  ]
  node [
    id 762
    label "YOON, C"
  ]
  node [
    id 763
    label "HAN, S"
  ]
  node [
    id 764
    label "TRUSINA, A"
  ]
  node [
    id 765
    label "MINNHAGEN, P"
  ]
  node [
    id 770
    label "HOLTER, N"
  ]
  node [
    id 771
    label "MITRA, M"
  ]
  node [
    id 772
    label "CIEPLAK, M"
  ]
  node [
    id 773
    label "FEDROFF, N"
  ]
  node [
    id 774
    label "HONG, H"
  ]
  node [
    id 775
    label "CHOI, M"
  ]
  node [
    id 776
    label "PARK, H"
  ]
  node [
    id 788
    label "LOPEZRUIZ, R"
  ]
  node [
    id 839
    label "MASON, S"
  ]
  node [
    id 840
    label "TOMBOR, B"
  ]
  node [
    id 853
    label "JIN, E"
  ]
  node [
    id 863
    label "JUNG, S"
  ]
  node [
    id 864
    label "KIM, S"
  ]
  node [
    id 865
    label "PARK, Y"
  ]
  node [
    id 866
    label "KALAPALA, V"
  ]
  node [
    id 867
    label "SANWALANI, V"
  ]
  node [
    id 892
    label "CHUNG, J"
  ]
  node [
    id 893
    label "KIM, J"
  ]
  node [
    id 894
    label "KINNEY, R"
  ]
  node [
    id 908
    label "KUMAR, S"
  ]
  node [
    id 934
    label "LEYVRAZ, F"
  ]
  node [
    id 944
    label "SIVAKUMAR, D"
  ]
  node [
    id 945
    label "UPFAL, E"
  ]
  node [
    id 955
    label "LAHTINEN, J"
  ]
  node [
    id 956
    label "KASKI, K"
  ]
  node [
    id 963
    label "LEONE, M"
  ]
  node [
    id 964
    label "ZECCHINA, R"
  ]
  node [
    id 977
    label "ABERG, Y"
  ]
  node [
    id 983
    label "LIU, Z"
  ]
  node [
    id 984
    label "LAI, Y"
  ]
  node [
    id 985
    label "HOPPENSTEADT, F"
  ]
  node [
    id 986
    label "YE, N"
  ]
  node [
    id 1005
    label "LUSSEAU, D"
  ]
  node [
    id 1008
    label "MACDONALD, P"
  ]
  node [
    id 1021
    label "RIGON, R"
  ]
  node [
    id 1022
    label "GIACOMETTI, A"
  ]
  node [
    id 1023
    label "RODRIGUEZITURBE, I"
  ]
  node [
    id 1024
    label "MARODI, M"
  ]
  node [
    id 1025
    label "DOVIDIO, F"
  ]
  node [
    id 1026
    label "MARRO, J"
  ]
  node [
    id 1027
    label "DICKMAN, R"
  ]
  node [
    id 1030
    label "ZALIZNYAK, A"
  ]
  node [
    id 1039
    label "MATTHEWS, P"
  ]
  node [
    id 1040
    label "MIROLLO, R"
  ]
  node [
    id 1041
    label "VALLONE, A"
  ]
  node [
    id 1081
    label "MONTOYA, J"
  ]
  node [
    id 1082
    label "MOREIRA, A"
  ]
  node [
    id 1083
    label "ANDRADE, J"
  ]
  node [
    id 1086
    label "GOMEZ, J"
  ]
  node [
    id 1087
    label "PACHECO, A"
  ]
  node [
    id 1088
    label "NEKOVEE, M"
  ]
  node [
    id 1089
    label "VAZQUEZPRADA, M"
  ]
  node [
    id 1091
    label "DASGUPTA, P"
  ]
  node [
    id 1092
    label "NISHIKAWA, T"
  ]
  node [
    id 1121
    label "FORREST, S"
  ]
  node [
    id 1122
    label "BALTHROP, J"
  ]
  node [
    id 1123
    label "LEICHT, E"
  ]
  node [
    id 1130
    label "RHO, K"
  ]
  node [
    id 1135
    label "ONNELA, J"
  ]
  node [
    id 1136
    label "CHAKRABORTI, A"
  ]
  node [
    id 1137
    label "KANTO, A"
  ]
  node [
    id 1138
    label "JARISARAMAKI, J"
  ]
  node [
    id 1145
    label "ROSENBLUM, M"
  ]
  node [
    id 1162
    label "BASSLER, K"
  ]
  node [
    id 1163
    label "CORRAL, A"
  ]
  node [
    id 1172
    label "PARK, J"
  ]
  node [
    id 1177
    label "RUBI, M"
  ]
  node [
    id 1178
    label "SMITH, E"
  ]
  node [
    id 1180
    label "PENNOCK, D"
  ]
  node [
    id 1181
    label "GLOVER, E"
  ]
  node [
    id 1182
    label "PETERMANNN, T"
  ]
  node [
    id 1189
    label "PLUCHINO, A"
  ]
  node [
    id 1190
    label "PODANI, J"
  ]
  node [
    id 1191
    label "SZATHMARY, E"
  ]
  node [
    id 1195
    label "PORTER, M"
  ]
  node [
    id 1196
    label "MUCHA, P"
  ]
  node [
    id 1197
    label "WARMBRAND, C"
  ]
  node [
    id 1214
    label "RADICCHI, F"
  ]
  node [
    id 1215
    label "CECCONI, F"
  ]
  node [
    id 1216
    label "LORETO, V"
  ]
  node [
    id 1217
    label "PARISI, D"
  ]
  node [
    id 1221
    label "RAMASCO, J"
  ]
  node [
    id 1228
    label "SOMERA, A"
  ]
  node [
    id 1229
    label "MONGRU, D"
  ]
  node [
    id 1239
    label "DARBYDOWMAN, K"
  ]
  node [
    id 1255
    label "ROSVALL, M"
  ]
  node [
    id 1263
    label "ROZENFELD, A"
  ]
  node [
    id 1282
    label "SCHAFER, C"
  ]
  node [
    id 1283
    label "ABEL, H"
  ]
  node [
    id 1295
    label "SCHWARTZ, N"
  ]
  node [
    id 1312
    label "SHEFI, O"
  ]
  node [
    id 1313
    label "GOLDING, I"
  ]
  node [
    id 1314
    label "SEGEV, R"
  ]
  node [
    id 1315
    label "BENJACOB, E"
  ]
  node [
    id 1316
    label "AYALI, A"
  ]
  node [
    id 1341
    label "SOFFER, S"
  ]
  node [
    id 1342
    label "KEPLER, T"
  ]
  node [
    id 1343
    label "SALAZARCIUDAD, I"
  ]
  node [
    id 1344
    label "GARCIAFERNANDEZ, J"
  ]
  node [
    id 1347
    label "SONG, C"
  ]
  node [
    id 1348
    label "MAKSE, H"
  ]
  node [
    id 1361
    label "AHARONY, A"
  ]
  node [
    id 1362
    label "ADLER, J"
  ]
  node [
    id 1363
    label "MEYERORTMANNS, H"
  ]
  node [
    id 1384
    label "SZABO, G"
  ]
  node [
    id 1385
    label "ALAVA, M"
  ]
  node [
    id 1389
    label "THURNER, S"
  ]
  node [
    id 1394
    label "TASS, P"
  ]
  node [
    id 1395
    label "WEULE, M"
  ]
  node [
    id 1396
    label "VOLKMANN, J"
  ]
  node [
    id 1397
    label "FREUND, H"
  ]
  node [
    id 1404
    label "TIERI, P"
  ]
  node [
    id 1405
    label "VALENSIN, S"
  ]
  node [
    id 1406
    label "CASTELLANI, G"
  ]
  node [
    id 1407
    label "REMONDINI, D"
  ]
  node [
    id 1408
    label "FRANCESCHI, C"
  ]
  node [
    id 1413
    label "KOZMA, B"
  ]
  node [
    id 1414
    label "HENGARTNER, N"
  ]
  node [
    id 1415
    label "KORNISS, G"
  ]
  node [
    id 1416
    label "TORRES, J"
  ]
  node [
    id 1417
    label "GARRIDO, P"
  ]
  node [
    id 1451
    label "CANCHO, R"
  ]
  node [
    id 1452
    label "VANNUCCHI, F"
  ]
  node [
    id 1460
    label "FLAMMINI, A"
  ]
  node [
    id 1461
    label "VAZQUEZ, F"
  ]
  node [
    id 1468
    label "CZIROK, A"
  ]
  node [
    id 1469
    label "COHEN, I"
  ]
  node [
    id 1470
    label "SHOCHET, O"
  ]
  node [
    id 1481
    label "VRAGOVIC, I"
  ]
  node [
    id 1482
    label "LOUIS, E"
  ]
  node [
    id 1529
    label "WUCHTY, S"
  ]
  node [
    id 1549
    label "YEUNG, M"
  ]
  node [
    id 1550
    label "YOOK, S"
  ]
  node [
    id 1551
    label "TU, Y"
  ]
  node [
    id 1556
    label "YUSONG, T"
  ]
  node [
    id 1557
    label "LINGJIANG, K"
  ]
  node [
    id 1558
    label "MUREN, L"
  ]
  node [
    id 1560
    label "ZAKS, M"
  ]
  node [
    id 1561
    label "PARK, E"
  ]
  edge [
    source 30
    target 32
    value 0.5
  ]
  edge [
    source 30
    target 33
    value 3.58333
  ]
  edge [
    source 30
    target 34
    value 1.58333
  ]
  edge [
    source 30
    target 131
    value 0.333333
  ]
  edge [
    source 30
    target 327
    value 0.333333
  ]
  edge [
    source 30
    target 840
    value 0.25
  ]
  edge [
    source 30
    target 402
    value 0.333333
  ]
  edge [
    source 30
    target 54
    value 0.25
  ]
  edge [
    source 30
    target 894
    value 0.333333
  ]
  edge [
    source 30
    target 31
    value 0.5
  ]
  edge [
    source 31
    target 32
    value 0.5
  ]
  edge [
    source 33
    target 131
    value 1.33333
  ]
  edge [
    source 33
    target 132
    value 2.275
  ]
  edge [
    source 33
    target 133
    value 1.025
  ]
  edge [
    source 33
    target 134
    value 0.525
  ]
  edge [
    source 33
    target 1550
    value 1.33333
  ]
  edge [
    source 33
    target 1295
    value 0.25
  ]
  edge [
    source 33
    target 34
    value 4.225
  ]
  edge [
    source 33
    target 1190
    value 0.2
  ]
  edge [
    source 33
    target 1191
    value 0.2
  ]
  edge [
    source 33
    target 561
    value 0.708333
  ]
  edge [
    source 33
    target 562
    value 0.458333
  ]
  edge [
    source 33
    target 51
    value 0.75
  ]
  edge [
    source 33
    target 52
    value 0.25
  ]
  edge [
    source 33
    target 53
    value 1.85833
  ]
  edge [
    source 33
    target 54
    value 2.99167
  ]
  edge [
    source 33
    target 190
    value 0.583333
  ]
  edge [
    source 33
    target 839
    value 0.333333
  ]
  edge [
    source 33
    target 840
    value 0.45
  ]
  edge [
    source 33
    target 1228
    value 0.25
  ]
  edge [
    source 33
    target 1229
    value 0.25
  ]
  edge [
    source 33
    target 464
    value 1
  ]
  edge [
    source 33
    target 377
    value 0.25
  ]
  edge [
    source 33
    target 1551
    value 0.333333
  ]
  edge [
    source 33
    target 485
    value 1
  ]
  edge [
    source 33
    target 488
    value 0.333333
  ]
  edge [
    source 33
    target 489
    value 0.333333
  ]
  edge [
    source 33
    target 1008
    value 0.5
  ]
  edge [
    source 33
    target 375
    value 0.25
  ]
  edge [
    source 33
    target 376
    value 0.25
  ]
  edge [
    source 33
    target 1529
    value 0.5
  ]
  edge [
    source 33
    target 507
    value 0.583333
  ]
  edge [
    source 33
    target 508
    value 0.583333
  ]
  edge [
    source 33
    target 509
    value 0.25
  ]
  edge [
    source 34
    target 131
    value 0.333333
  ]
  edge [
    source 34
    target 132
    value 0.525
  ]
  edge [
    source 34
    target 133
    value 1.025
  ]
  edge [
    source 34
    target 134
    value 0.525
  ]
  edge [
    source 34
    target 652
    value 0.25
  ]
  edge [
    source 34
    target 654
    value 1.25
  ]
  edge [
    source 34
    target 655
    value 0.25
  ]
  edge [
    source 34
    target 657
    value 0.25
  ]
  edge [
    source 34
    target 1190
    value 0.2
  ]
  edge [
    source 34
    target 1191
    value 0.2
  ]
  edge [
    source 34
    target 561
    value 0.375
  ]
  edge [
    source 34
    target 562
    value 0.125
  ]
  edge [
    source 34
    target 53
    value 0.775
  ]
  edge [
    source 34
    target 54
    value 1.15833
  ]
  edge [
    source 34
    target 839
    value 0.333333
  ]
  edge [
    source 34
    target 840
    value 0.45
  ]
  edge [
    source 34
    target 1550
    value 0.833333
  ]
  edge [
    source 34
    target 1551
    value 0.333333
  ]
  edge [
    source 34
    target 865
    value 0.5
  ]
  edge [
    source 34
    target 1130
    value 0.5
  ]
  edge [
    source 34
    target 756
    value 0.5
  ]
  edge [
    source 34
    target 760
    value 0.5
  ]
  edge [
    source 34
    target 761
    value 0.333333
  ]
  edge [
    source 34
    target 762
    value 0.333333
  ]
  edge [
    source 34
    target 763
    value 0.333333
  ]
  edge [
    source 44
    target 45
    value 0.5
  ]
  edge [
    source 44
    target 46
    value 0.5
  ]
  edge [
    source 45
    target 609
    value 0.833333
  ]
  edge [
    source 45
    target 610
    value 0.5
  ]
  edge [
    source 45
    target 611
    value 0.333333
  ]
  edge [
    source 45
    target 612
    value 0.333333
  ]
  edge [
    source 45
    target 46
    value 0.5
  ]
  edge [
    source 46
    target 192
    value 0.333333
  ]
  edge [
    source 46
    target 193
    value 0.333333
  ]
  edge [
    source 46
    target 194
    value 0.5
  ]
  edge [
    source 46
    target 428
    value 1.33333
  ]
  edge [
    source 46
    target 78
    value 1
  ]
  edge [
    source 46
    target 1361
    value 1.33333
  ]
  edge [
    source 46
    target 1362
    value 0.333333
  ]
  edge [
    source 46
    target 1363
    value 1
  ]
  edge [
    source 46
    target 596
    value 1
  ]
  edge [
    source 46
    target 191
    value 0.833333
  ]
  edge [
    source 47
    target 48
    value 0.333333
  ]
  edge [
    source 47
    target 49
    value 0.333333
  ]
  edge [
    source 47
    target 50
    value 0.333333
  ]
  edge [
    source 48
    target 49
    value 0.333333
  ]
  edge [
    source 48
    target 50
    value 0.333333
  ]
  edge [
    source 48
    target 216
    value 0.333333
  ]
  edge [
    source 48
    target 217
    value 0.333333
  ]
  edge [
    source 48
    target 218
    value 0.333333
  ]
  edge [
    source 49
    target 50
    value 0.333333
  ]
  edge [
    source 51
    target 1008
    value 0.5
  ]
  edge [
    source 51
    target 52
    value 0.25
  ]
  edge [
    source 51
    target 53
    value 0.25
  ]
  edge [
    source 51
    target 54
    value 0.25
  ]
  edge [
    source 51
    target 55
    value 0.5
  ]
  edge [
    source 51
    target 56
    value 0.5
  ]
  edge [
    source 51
    target 57
    value 1
  ]
  edge [
    source 51
    target 58
    value 1
  ]
  edge [
    source 52
    target 53
    value 0.25
  ]
  edge [
    source 52
    target 54
    value 0.25
  ]
  edge [
    source 53
    target 1024
    value 0.5
  ]
  edge [
    source 53
    target 1315
    value 0.25
  ]
  edge [
    source 53
    target 132
    value 1.025
  ]
  edge [
    source 53
    target 133
    value 0.525
  ]
  edge [
    source 53
    target 134
    value 0.525
  ]
  edge [
    source 53
    target 1025
    value 0.5
  ]
  edge [
    source 53
    target 561
    value 0.708333
  ]
  edge [
    source 53
    target 562
    value 0.458333
  ]
  edge [
    source 53
    target 54
    value 0.625
  ]
  edge [
    source 53
    target 1468
    value 0.25
  ]
  edge [
    source 53
    target 1469
    value 0.25
  ]
  edge [
    source 53
    target 1470
    value 0.25
  ]
  edge [
    source 54
    target 132
    value 0.375
  ]
  edge [
    source 54
    target 133
    value 0.125
  ]
  edge [
    source 54
    target 134
    value 0.125
  ]
  edge [
    source 54
    target 839
    value 0.333333
  ]
  edge [
    source 54
    target 488
    value 0.333333
  ]
  edge [
    source 54
    target 489
    value 0.333333
  ]
  edge [
    source 54
    target 1191
    value 0.2
  ]
  edge [
    source 54
    target 1228
    value 0.25
  ]
  edge [
    source 54
    target 1229
    value 0.25
  ]
  edge [
    source 54
    target 1550
    value 0.5
  ]
  edge [
    source 54
    target 840
    value 0.45
  ]
  edge [
    source 54
    target 562
    value 0.125
  ]
  edge [
    source 54
    target 561
    value 0.375
  ]
  edge [
    source 54
    target 1529
    value 0.5
  ]
  edge [
    source 54
    target 1190
    value 0.2
  ]
  edge [
    source 55
    target 547
    value 0.5
  ]
  edge [
    source 55
    target 934
    value 0.5
  ]
  edge [
    source 55
    target 654
    value 0.333333
  ]
  edge [
    source 55
    target 56
    value 3.83333
  ]
  edge [
    source 55
    target 1461
    value 0.5
  ]
  edge [
    source 55
    target 184
    value 0.5
  ]
  edge [
    source 55
    target 90
    value 1
  ]
  edge [
    source 55
    target 893
    value 0.333333
  ]
  edge [
    source 56
    target 547
    value 0.5
  ]
  edge [
    source 56
    target 934
    value 0.5
  ]
  edge [
    source 56
    target 654
    value 0.333333
  ]
  edge [
    source 56
    target 1461
    value 0.5
  ]
  edge [
    source 56
    target 184
    value 0.5
  ]
  edge [
    source 56
    target 893
    value 0.333333
  ]
  edge [
    source 57
    target 58
    value 1
  ]
  edge [
    source 57
    target 685
    value 1
  ]
  edge [
    source 69
    target 97
    value 1.83333
  ]
  edge [
    source 69
    target 310
    value 0.5
  ]
  edge [
    source 69
    target 709
    value 0.666667
  ]
  edge [
    source 69
    target 70
    value 0.833333
  ]
  edge [
    source 69
    target 71
    value 2.16667
  ]
  edge [
    source 69
    target 72
    value 0.916667
  ]
  edge [
    source 69
    target 710
    value 0.333333
  ]
  edge [
    source 69
    target 977
    value 0.25
  ]
  edge [
    source 69
    target 757
    value 0.75
  ]
  edge [
    source 69
    target 758
    value 0.75
  ]
  edge [
    source 69
    target 1082
    value 0.5
  ]
  edge [
    source 69
    target 1083
    value 0.5
  ]
  edge [
    source 70
    target 72
    value 0.333333
  ]
  edge [
    source 70
    target 71
    value 0.833333
  ]
  edge [
    source 71
    target 736
    value 0.5
  ]
  edge [
    source 71
    target 737
    value 0.5
  ]
  edge [
    source 71
    target 72
    value 0.666667
  ]
  edge [
    source 71
    target 149
    value 1.16667
  ]
  edge [
    source 71
    target 150
    value 0.666667
  ]
  edge [
    source 71
    target 151
    value 1.16667
  ]
  edge [
    source 71
    target 157
    value 0.5
  ]
  edge [
    source 71
    target 158
    value 0.5
  ]
  edge [
    source 71
    target 709
    value 0.333333
  ]
  edge [
    source 72
    target 738
    value 0.5
  ]
  edge [
    source 72
    target 709
    value 0.333333
  ]
  edge [
    source 72
    target 235
    value 1
  ]
  edge [
    source 72
    target 977
    value 0.25
  ]
  edge [
    source 72
    target 757
    value 0.25
  ]
  edge [
    source 72
    target 758
    value 0.25
  ]
  edge [
    source 72
    target 443
    value 0.5
  ]
  edge [
    source 77
    target 80
    value 0.333333
  ]
  edge [
    source 77
    target 78
    value 0.333333
  ]
  edge [
    source 77
    target 79
    value 0.333333
  ]
  edge [
    source 78
    target 641
    value 1
  ]
  edge [
    source 78
    target 646
    value 2.5
  ]
  edge [
    source 78
    target 1172
    value 1
  ]
  edge [
    source 78
    target 281
    value 1
  ]
  edge [
    source 78
    target 1195
    value 0.333333
  ]
  edge [
    source 78
    target 1196
    value 0.333333
  ]
  edge [
    source 78
    target 1197
    value 0.333333
  ]
  edge [
    source 78
    target 305
    value 0.583333
  ]
  edge [
    source 78
    target 306
    value 0.25
  ]
  edge [
    source 78
    target 307
    value 0.25
  ]
  edge [
    source 78
    target 308
    value 1.58333
  ]
  edge [
    source 78
    target 309
    value 3.33333
  ]
  edge [
    source 78
    target 79
    value 0.333333
  ]
  edge [
    source 78
    target 80
    value 0.333333
  ]
  edge [
    source 78
    target 853
    value 0.5
  ]
  edge [
    source 78
    target 1121
    value 0.5
  ]
  edge [
    source 78
    target 1122
    value 0.5
  ]
  edge [
    source 78
    target 1123
    value 0.5
  ]
  edge [
    source 78
    target 490
    value 0.5
  ]
  edge [
    source 78
    target 1005
    value 1
  ]
  edge [
    source 78
    target 370
    value 0.5
  ]
  edge [
    source 78
    target 371
    value 2.5
  ]
  edge [
    source 78
    target 756
    value 0.5
  ]
  edge [
    source 78
    target 759
    value 0.5
  ]
  edge [
    source 78
    target 121
    value 1
  ]
  edge [
    source 79
    target 80
    value 0.333333
  ]
  edge [
    source 94
    target 96
    value 2.66667
  ]
  edge [
    source 94
    target 97
    value 2.33333
  ]
  edge [
    source 94
    target 98
    value 0.5
  ]
  edge [
    source 94
    target 99
    value 0.5
  ]
  edge [
    source 94
    target 100
    value 0.25
  ]
  edge [
    source 94
    target 225
    value 0.333333
  ]
  edge [
    source 94
    target 150
    value 0.333333
  ]
  edge [
    source 94
    target 708
    value 0.583333
  ]
  edge [
    source 94
    target 95
    value 0.5
  ]
  edge [
    source 95
    target 96
    value 0.5
  ]
  edge [
    source 95
    target 97
    value 0.5
  ]
  edge [
    source 95
    target 98
    value 0.5
  ]
  edge [
    source 96
    target 97
    value 2.33333
  ]
  edge [
    source 96
    target 98
    value 0.5
  ]
  edge [
    source 96
    target 99
    value 0.5
  ]
  edge [
    source 96
    target 100
    value 0.25
  ]
  edge [
    source 96
    target 225
    value 0.333333
  ]
  edge [
    source 96
    target 1481
    value 0.5
  ]
  edge [
    source 96
    target 1482
    value 0.5
  ]
  edge [
    source 96
    target 708
    value 0.583333
  ]
  edge [
    source 96
    target 150
    value 0.833333
  ]
  edge [
    source 96
    target 1177
    value 0.5
  ]
  edge [
    source 96
    target 700
    value 0.333333
  ]
  edge [
    source 96
    target 701
    value 0.333333
  ]
  edge [
    source 96
    target 702
    value 0.333333
  ]
  edge [
    source 97
    target 98
    value 0.5
  ]
  edge [
    source 97
    target 99
    value 0.5
  ]
  edge [
    source 97
    target 100
    value 0.25
  ]
  edge [
    source 97
    target 710
    value 0.333333
  ]
  edge [
    source 97
    target 310
    value 0.5
  ]
  edge [
    source 97
    target 708
    value 0.583333
  ]
  edge [
    source 97
    target 709
    value 0.333333
  ]
  edge [
    source 99
    target 100
    value 1.25
  ]
  edge [
    source 99
    target 708
    value 0.25
  ]
  edge [
    source 113
    target 114
    value 1
  ]
  edge [
    source 114
    target 1162
    value 0.5
  ]
  edge [
    source 114
    target 1163
    value 0.5
  ]
  edge [
    source 120
    target 121
    value 1
  ]
  edge [
    source 121
    target 548
    value 0.333333
  ]
  edge [
    source 121
    target 549
    value 0.333333
  ]
  edge [
    source 121
    target 550
    value 1.83333
  ]
  edge [
    source 121
    target 1255
    value 0.833333
  ]
  edge [
    source 121
    target 1030
    value 0.5
  ]
  edge [
    source 121
    target 764
    value 0.833333
  ]
  edge [
    source 121
    target 765
    value 0.333333
  ]
  edge [
    source 126
    target 128
    value 0.5
  ]
  edge [
    source 126
    target 770
    value 0.2
  ]
  edge [
    source 126
    target 771
    value 0.2
  ]
  edge [
    source 126
    target 772
    value 0.2
  ]
  edge [
    source 126
    target 773
    value 0.2
  ]
  edge [
    source 126
    target 127
    value 0.7
  ]
  edge [
    source 127
    target 128
    value 0.75
  ]
  edge [
    source 127
    target 770
    value 0.2
  ]
  edge [
    source 127
    target 771
    value 0.2
  ]
  edge [
    source 127
    target 772
    value 0.2
  ]
  edge [
    source 127
    target 517
    value 0.333333
  ]
  edge [
    source 127
    target 1460
    value 0.333333
  ]
  edge [
    source 127
    target 151
    value 0.333333
  ]
  edge [
    source 127
    target 773
    value 0.2
  ]
  edge [
    source 127
    target 1021
    value 0.25
  ]
  edge [
    source 127
    target 1022
    value 0.25
  ]
  edge [
    source 127
    target 1023
    value 0.25
  ]
  edge [
    source 128
    target 1021
    value 0.75
  ]
  edge [
    source 128
    target 1022
    value 0.25
  ]
  edge [
    source 128
    target 1023
    value 1.75
  ]
  edge [
    source 131
    target 203
    value 1
  ]
  edge [
    source 132
    target 133
    value 0.525
  ]
  edge [
    source 132
    target 134
    value 0.525
  ]
  edge [
    source 132
    target 1228
    value 0.25
  ]
  edge [
    source 132
    target 1229
    value 0.25
  ]
  edge [
    source 132
    target 561
    value 0.125
  ]
  edge [
    source 132
    target 562
    value 0.125
  ]
  edge [
    source 133
    target 134
    value 0.525
  ]
  edge [
    source 133
    target 561
    value 0.125
  ]
  edge [
    source 133
    target 562
    value 0.125
  ]
  edge [
    source 134
    target 561
    value 0.125
  ]
  edge [
    source 134
    target 562
    value 0.125
  ]
  edge [
    source 135
    target 136
    value 1
  ]
  edge [
    source 136
    target 585
    value 0.333333
  ]
  edge [
    source 136
    target 586
    value 0.333333
  ]
  edge [
    source 136
    target 587
    value 1.83333
  ]
  edge [
    source 136
    target 216
    value 0.5
  ]
  edge [
    source 136
    target 729
    value 0.5
  ]
  edge [
    source 136
    target 223
    value 0.5
  ]
  edge [
    source 149
    target 152
    value 1
  ]
  edge [
    source 149
    target 150
    value 0.666667
  ]
  edge [
    source 149
    target 151
    value 1.16667
  ]
  edge [
    source 150
    target 225
    value 2.08333
  ]
  edge [
    source 150
    target 516
    value 1.08333
  ]
  edge [
    source 150
    target 517
    value 1.58333
  ]
  edge [
    source 150
    target 301
    value 0.5
  ]
  edge [
    source 150
    target 500
    value 0.5
  ]
  edge [
    source 150
    target 151
    value 4.75
  ]
  edge [
    source 150
    target 1177
    value 0.5
  ]
  edge [
    source 150
    target 1178
    value 0.833333
  ]
  edge [
    source 150
    target 281
    value 1.83333
  ]
  edge [
    source 150
    target 1342
    value 0.333333
  ]
  edge [
    source 150
    target 1221
    value 0.5
  ]
  edge [
    source 151
    target 1088
    value 0.5
  ]
  edge [
    source 151
    target 225
    value 0.75
  ]
  edge [
    source 151
    target 963
    value 0.333333
  ]
  edge [
    source 151
    target 516
    value 1.58333
  ]
  edge [
    source 151
    target 517
    value 2.25
  ]
  edge [
    source 151
    target 330
    value 0.5
  ]
  edge [
    source 151
    target 331
    value 0.5
  ]
  edge [
    source 151
    target 301
    value 0.5
  ]
  edge [
    source 151
    target 1460
    value 0.333333
  ]
  edge [
    source 151
    target 964
    value 0.333333
  ]
  edge [
    source 152
    target 517
    value 1
  ]
  edge [
    source 157
    target 158
    value 0.5
  ]
  edge [
    source 162
    target 163
    value 1
  ]
  edge [
    source 162
    target 316
    value 0.25
  ]
  edge [
    source 162
    target 301
    value 0.25
  ]
  edge [
    source 162
    target 638
    value 0.25
  ]
  edge [
    source 162
    target 639
    value 0.25
  ]
  edge [
    source 184
    target 185
    value 0.5
  ]
  edge [
    source 184
    target 186
    value 0.5
  ]
  edge [
    source 185
    target 186
    value 0.5
  ]
  edge [
    source 186
    target 1413
    value 0.25
  ]
  edge [
    source 186
    target 1414
    value 0.25
  ]
  edge [
    source 186
    target 1415
    value 0.25
  ]
  edge [
    source 186
    target 1162
    value 1.25
  ]
  edge [
    source 190
    target 507
    value 0.583333
  ]
  edge [
    source 190
    target 508
    value 0.583333
  ]
  edge [
    source 190
    target 509
    value 0.25
  ]
  edge [
    source 191
    target 192
    value 0.333333
  ]
  edge [
    source 191
    target 193
    value 0.333333
  ]
  edge [
    source 191
    target 194
    value 0.5
  ]
  edge [
    source 192
    target 193
    value 0.333333
  ]
  edge [
    source 194
    target 1384
    value 0.5
  ]
  edge [
    source 194
    target 1385
    value 0.5
  ]
  edge [
    source 194
    target 1135
    value 0.583333
  ]
  edge [
    source 194
    target 1136
    value 0.25
  ]
  edge [
    source 194
    target 1137
    value 0.25
  ]
  edge [
    source 194
    target 1138
    value 0.333333
  ]
  edge [
    source 194
    target 955
    value 0.5
  ]
  edge [
    source 194
    target 956
    value 1.08333
  ]
  edge [
    source 203
    target 301
    value 1.16667
  ]
  edge [
    source 203
    target 302
    value 0.833333
  ]
  edge [
    source 203
    target 303
    value 0.333333
  ]
  edge [
    source 203
    target 316
    value 0.333333
  ]
  edge [
    source 203
    target 317
    value 0.333333
  ]
  edge [
    source 216
    target 224
    value 0.583333
  ]
  edge [
    source 216
    target 347
    value 0.583333
  ]
  edge [
    source 216
    target 516
    value 0.333333
  ]
  edge [
    source 216
    target 220
    value 1.5
  ]
  edge [
    source 216
    target 1452
    value 1
  ]
  edge [
    source 216
    target 221
    value 0.25
  ]
  edge [
    source 216
    target 1041
    value 0.333333
  ]
  edge [
    source 216
    target 788
    value 0.333333
  ]
  edge [
    source 216
    target 219
    value 0.5
  ]
  edge [
    source 216
    target 217
    value 1.08333
  ]
  edge [
    source 216
    target 345
    value 0.583333
  ]
  edge [
    source 216
    target 346
    value 0.916667
  ]
  edge [
    source 216
    target 251
    value 0.25
  ]
  edge [
    source 216
    target 252
    value 0.5
  ]
  edge [
    source 216
    target 218
    value 1.66667
  ]
  edge [
    source 216
    target 222
    value 0.25
  ]
  edge [
    source 216
    target 223
    value 0.5
  ]
  edge [
    source 217
    target 218
    value 1.08333
  ]
  edge [
    source 217
    target 251
    value 0.25
  ]
  edge [
    source 217
    target 252
    value 0.25
  ]
  edge [
    source 218
    target 224
    value 0.583333
  ]
  edge [
    source 218
    target 219
    value 0.25
  ]
  edge [
    source 218
    target 220
    value 0.25
  ]
  edge [
    source 218
    target 1041
    value 0.333333
  ]
  edge [
    source 218
    target 251
    value 0.25
  ]
  edge [
    source 218
    target 252
    value 0.25
  ]
  edge [
    source 219
    target 224
    value 0.25
  ]
  edge [
    source 219
    target 1282
    value 0.333333
  ]
  edge [
    source 219
    target 1283
    value 0.333333
  ]
  edge [
    source 219
    target 343
    value 2.47619
  ]
  edge [
    source 219
    target 222
    value 1.75
  ]
  edge [
    source 219
    target 1394
    value 0.142857
  ]
  edge [
    source 219
    target 1395
    value 0.142857
  ]
  edge [
    source 219
    target 1396
    value 0.142857
  ]
  edge [
    source 219
    target 1145
    value 2.14286
  ]
  edge [
    source 219
    target 697
    value 0.142857
  ]
  edge [
    source 219
    target 1560
    value 0.333333
  ]
  edge [
    source 219
    target 1561
    value 0.333333
  ]
  edge [
    source 219
    target 220
    value 0.5
  ]
  edge [
    source 219
    target 221
    value 0.583333
  ]
  edge [
    source 219
    target 473
    value 0.5
  ]
  edge [
    source 219
    target 1397
    value 0.142857
  ]
  edge [
    source 220
    target 224
    value 0.25
  ]
  edge [
    source 220
    target 221
    value 0.25
  ]
  edge [
    source 220
    target 222
    value 0.25
  ]
  edge [
    source 221
    target 343
    value 0.333333
  ]
  edge [
    source 221
    target 1145
    value 0.333333
  ]
  edge [
    source 221
    target 222
    value 0.25
  ]
  edge [
    source 222
    target 473
    value 0.5
  ]
  edge [
    source 224
    target 1041
    value 0.333333
  ]
  edge [
    source 225
    target 516
    value 0.25
  ]
  edge [
    source 225
    target 517
    value 0.25
  ]
  edge [
    source 233
    target 234
    value 0.5
  ]
  edge [
    source 233
    target 235
    value 0.5
  ]
  edge [
    source 234
    target 235
    value 0.5
  ]
  edge [
    source 251
    target 252
    value 0.25
  ]
  edge [
    source 252
    target 345
    value 0.25
  ]
  edge [
    source 252
    target 347
    value 0.25
  ]
  edge [
    source 252
    target 346
    value 0.25
  ]
  edge [
    source 262
    target 263
    value 0.142857
  ]
  edge [
    source 262
    target 264
    value 0.142857
  ]
  edge [
    source 262
    target 265
    value 0.142857
  ]
  edge [
    source 262
    target 266
    value 0.142857
  ]
  edge [
    source 262
    target 267
    value 0.142857
  ]
  edge [
    source 262
    target 268
    value 0.142857
  ]
  edge [
    source 262
    target 269
    value 0.142857
  ]
  edge [
    source 263
    target 264
    value 0.142857
  ]
  edge [
    source 263
    target 265
    value 0.67619
  ]
  edge [
    source 263
    target 266
    value 0.67619
  ]
  edge [
    source 263
    target 267
    value 0.142857
  ]
  edge [
    source 263
    target 268
    value 0.67619
  ]
  edge [
    source 263
    target 269
    value 0.142857
  ]
  edge [
    source 263
    target 944
    value 0.2
  ]
  edge [
    source 263
    target 945
    value 0.2
  ]
  edge [
    source 264
    target 265
    value 0.142857
  ]
  edge [
    source 264
    target 266
    value 0.142857
  ]
  edge [
    source 264
    target 267
    value 0.142857
  ]
  edge [
    source 264
    target 268
    value 0.142857
  ]
  edge [
    source 264
    target 269
    value 0.142857
  ]
  edge [
    source 265
    target 268
    value 0.92619
  ]
  edge [
    source 265
    target 266
    value 0.92619
  ]
  edge [
    source 265
    target 267
    value 0.142857
  ]
  edge [
    source 265
    target 908
    value 0.25
  ]
  edge [
    source 265
    target 269
    value 0.142857
  ]
  edge [
    source 265
    target 944
    value 0.2
  ]
  edge [
    source 265
    target 945
    value 0.2
  ]
  edge [
    source 265
    target 307
    value 0.25
  ]
  edge [
    source 266
    target 267
    value 0.142857
  ]
  edge [
    source 266
    target 908
    value 0.25
  ]
  edge [
    source 266
    target 269
    value 0.142857
  ]
  edge [
    source 266
    target 944
    value 0.2
  ]
  edge [
    source 266
    target 945
    value 0.2
  ]
  edge [
    source 266
    target 307
    value 0.25
  ]
  edge [
    source 266
    target 268
    value 0.92619
  ]
  edge [
    source 267
    target 268
    value 0.142857
  ]
  edge [
    source 267
    target 269
    value 0.142857
  ]
  edge [
    source 268
    target 908
    value 0.25
  ]
  edge [
    source 268
    target 269
    value 0.142857
  ]
  edge [
    source 268
    target 944
    value 0.2
  ]
  edge [
    source 268
    target 945
    value 0.2
  ]
  edge [
    source 268
    target 307
    value 0.25
  ]
  edge [
    source 276
    target 277
    value 0.5
  ]
  edge [
    source 276
    target 278
    value 0.5
  ]
  edge [
    source 277
    target 401
    value 0.166667
  ]
  edge [
    source 277
    target 402
    value 0.166667
  ]
  edge [
    source 277
    target 403
    value 0.5
  ]
  edge [
    source 277
    target 404
    value 0.166667
  ]
  edge [
    source 277
    target 405
    value 0.166667
  ]
  edge [
    source 277
    target 278
    value 1
  ]
  edge [
    source 277
    target 595
    value 0.333333
  ]
  edge [
    source 278
    target 401
    value 0.166667
  ]
  edge [
    source 278
    target 402
    value 0.166667
  ]
  edge [
    source 278
    target 403
    value 0.5
  ]
  edge [
    source 278
    target 404
    value 0.166667
  ]
  edge [
    source 278
    target 595
    value 0.333333
  ]
  edge [
    source 278
    target 405
    value 0.166667
  ]
  edge [
    source 279
    target 280
    value 0.166667
  ]
  edge [
    source 279
    target 281
    value 0.166667
  ]
  edge [
    source 279
    target 282
    value 0.166667
  ]
  edge [
    source 279
    target 283
    value 0.166667
  ]
  edge [
    source 279
    target 284
    value 0.166667
  ]
  edge [
    source 279
    target 285
    value 0.166667
  ]
  edge [
    source 280
    target 281
    value 0.166667
  ]
  edge [
    source 280
    target 282
    value 0.166667
  ]
  edge [
    source 280
    target 283
    value 0.166667
  ]
  edge [
    source 280
    target 284
    value 0.166667
  ]
  edge [
    source 280
    target 285
    value 0.166667
  ]
  edge [
    source 281
    target 576
    value 0.5
  ]
  edge [
    source 281
    target 1344
    value 0.5
  ]
  edge [
    source 281
    target 1451
    value 0.5
  ]
  edge [
    source 281
    target 285
    value 0.166667
  ]
  edge [
    source 281
    target 575
    value 0.5
  ]
  edge [
    source 281
    target 574
    value 2.5
  ]
  edge [
    source 281
    target 1081
    value 2
  ]
  edge [
    source 281
    target 1178
    value 0.833333
  ]
  edge [
    source 281
    target 283
    value 3.16667
  ]
  edge [
    source 281
    target 284
    value 0.166667
  ]
  edge [
    source 281
    target 282
    value 0.166667
  ]
  edge [
    source 281
    target 1342
    value 0.333333
  ]
  edge [
    source 281
    target 1343
    value 0.5
  ]
  edge [
    source 282
    target 450
    value 1
  ]
  edge [
    source 282
    target 283
    value 0.166667
  ]
  edge [
    source 282
    target 284
    value 0.166667
  ]
  edge [
    source 282
    target 285
    value 0.166667
  ]
  edge [
    source 283
    target 1451
    value 0.5
  ]
  edge [
    source 283
    target 284
    value 0.166667
  ]
  edge [
    source 283
    target 285
    value 0.166667
  ]
  edge [
    source 283
    target 574
    value 0.5
  ]
  edge [
    source 284
    target 285
    value 0.166667
  ]
  edge [
    source 301
    target 302
    value 1.33333
  ]
  edge [
    source 301
    target 303
    value 0.333333
  ]
  edge [
    source 301
    target 304
    value 0.5
  ]
  edge [
    source 301
    target 463
    value 0.5
  ]
  edge [
    source 301
    target 316
    value 0.583333
  ]
  edge [
    source 301
    target 317
    value 0.333333
  ]
  edge [
    source 301
    target 638
    value 0.75
  ]
  edge [
    source 301
    target 639
    value 0.25
  ]
  edge [
    source 302
    target 304
    value 0.5
  ]
  edge [
    source 302
    target 1182
    value 1
  ]
  edge [
    source 302
    target 303
    value 0.333333
  ]
  edge [
    source 303
    target 1026
    value 0.333333
  ]
  edge [
    source 303
    target 1416
    value 0.333333
  ]
  edge [
    source 303
    target 1417
    value 0.333333
  ]
  edge [
    source 303
    target 499
    value 1
  ]
  edge [
    source 305
    target 306
    value 0.25
  ]
  edge [
    source 305
    target 307
    value 0.25
  ]
  edge [
    source 305
    target 308
    value 0.583333
  ]
  edge [
    source 305
    target 309
    value 0.333333
  ]
  edge [
    source 306
    target 307
    value 0.25
  ]
  edge [
    source 306
    target 308
    value 0.25
  ]
  edge [
    source 307
    target 908
    value 0.25
  ]
  edge [
    source 307
    target 308
    value 0.25
  ]
  edge [
    source 307
    target 590
    value 1
  ]
  edge [
    source 308
    target 1549
    value 1
  ]
  edge [
    source 308
    target 1039
    value 0.5
  ]
  edge [
    source 308
    target 1040
    value 1.5
  ]
  edge [
    source 308
    target 309
    value 2.33333
  ]
  edge [
    source 309
    target 490
    value 1.5
  ]
  edge [
    source 309
    target 491
    value 0.5
  ]
  edge [
    source 309
    target 493
    value 0.5
  ]
  edge [
    source 309
    target 371
    value 0.5
  ]
  edge [
    source 316
    target 317
    value 0.333333
  ]
  edge [
    source 316
    target 638
    value 0.25
  ]
  edge [
    source 316
    target 639
    value 0.25
  ]
  edge [
    source 326
    target 328
    value 0.333333
  ]
  edge [
    source 326
    target 329
    value 0.333333
  ]
  edge [
    source 326
    target 327
    value 0.333333
  ]
  edge [
    source 327
    target 416
    value 3.5
  ]
  edge [
    source 327
    target 417
    value 1
  ]
  edge [
    source 327
    target 1189
    value 0.5
  ]
  edge [
    source 327
    target 328
    value 1.16667
  ]
  edge [
    source 327
    target 329
    value 0.333333
  ]
  edge [
    source 327
    target 402
    value 2.16667
  ]
  edge [
    source 327
    target 1408
    value 0.166667
  ]
  edge [
    source 327
    target 596
    value 0.5
  ]
  edge [
    source 327
    target 894
    value 0.333333
  ]
  edge [
    source 327
    target 1404
    value 0.166667
  ]
  edge [
    source 327
    target 1405
    value 0.166667
  ]
  edge [
    source 327
    target 1406
    value 0.166667
  ]
  edge [
    source 327
    target 1407
    value 0.166667
  ]
  edge [
    source 328
    target 416
    value 0.333333
  ]
  edge [
    source 328
    target 1189
    value 0.5
  ]
  edge [
    source 328
    target 329
    value 0.333333
  ]
  edge [
    source 328
    target 402
    value 0.333333
  ]
  edge [
    source 329
    target 547
    value 1.5
  ]
  edge [
    source 329
    target 1389
    value 1.5
  ]
  edge [
    source 330
    target 1216
    value 0.25
  ]
  edge [
    source 330
    target 1217
    value 0.25
  ]
  edge [
    source 330
    target 331
    value 0.5
  ]
  edge [
    source 330
    target 1214
    value 0.25
  ]
  edge [
    source 330
    target 1215
    value 0.25
  ]
  edge [
    source 342
    target 344
    value 0.5
  ]
  edge [
    source 342
    target 692
    value 1
  ]
  edge [
    source 342
    target 343
    value 0.5
  ]
  edge [
    source 343
    target 1394
    value 0.142857
  ]
  edge [
    source 343
    target 1395
    value 0.142857
  ]
  edge [
    source 343
    target 1396
    value 0.142857
  ]
  edge [
    source 343
    target 1397
    value 0.142857
  ]
  edge [
    source 343
    target 697
    value 0.142857
  ]
  edge [
    source 343
    target 344
    value 0.5
  ]
  edge [
    source 343
    target 1145
    value 1.47619
  ]
  edge [
    source 345
    target 346
    value 0.583333
  ]
  edge [
    source 345
    target 347
    value 0.583333
  ]
  edge [
    source 346
    target 516
    value 0.333333
  ]
  edge [
    source 346
    target 788
    value 0.333333
  ]
  edge [
    source 346
    target 347
    value 0.583333
  ]
  edge [
    source 370
    target 371
    value 0.5
  ]
  edge [
    source 371
    target 866
    value 0.5
  ]
  edge [
    source 371
    target 867
    value 0.5
  ]
  edge [
    source 371
    target 759
    value 0.5
  ]
  edge [
    source 375
    target 1295
    value 0.25
  ]
  edge [
    source 375
    target 376
    value 1.91667
  ]
  edge [
    source 375
    target 377
    value 2.91667
  ]
  edge [
    source 375
    target 378
    value 0.333333
  ]
  edge [
    source 375
    target 1263
    value 0.333333
  ]
  edge [
    source 376
    target 1263
    value 0.333333
  ]
  edge [
    source 376
    target 377
    value 1.91667
  ]
  edge [
    source 376
    target 378
    value 0.333333
  ]
  edge [
    source 376
    target 1295
    value 0.25
  ]
  edge [
    source 377
    target 1347
    value 0.5
  ]
  edge [
    source 377
    target 1348
    value 0.5
  ]
  edge [
    source 377
    target 1263
    value 0.333333
  ]
  edge [
    source 377
    target 378
    value 0.333333
  ]
  edge [
    source 377
    target 1295
    value 0.25
  ]
  edge [
    source 401
    target 402
    value 0.166667
  ]
  edge [
    source 401
    target 403
    value 0.166667
  ]
  edge [
    source 401
    target 404
    value 0.166667
  ]
  edge [
    source 401
    target 405
    value 0.166667
  ]
  edge [
    source 402
    target 416
    value 0.833333
  ]
  edge [
    source 402
    target 417
    value 1
  ]
  edge [
    source 402
    target 403
    value 0.166667
  ]
  edge [
    source 402
    target 404
    value 0.166667
  ]
  edge [
    source 402
    target 894
    value 0.333333
  ]
  edge [
    source 402
    target 405
    value 0.166667
  ]
  edge [
    source 403
    target 595
    value 0.333333
  ]
  edge [
    source 403
    target 404
    value 0.166667
  ]
  edge [
    source 403
    target 405
    value 0.166667
  ]
  edge [
    source 404
    target 405
    value 0.166667
  ]
  edge [
    source 416
    target 1408
    value 0.166667
  ]
  edge [
    source 416
    target 596
    value 0.5
  ]
  edge [
    source 416
    target 1404
    value 0.166667
  ]
  edge [
    source 416
    target 1405
    value 0.166667
  ]
  edge [
    source 416
    target 1406
    value 0.166667
  ]
  edge [
    source 416
    target 1407
    value 0.166667
  ]
  edge [
    source 428
    target 1361
    value 0.333333
  ]
  edge [
    source 428
    target 1362
    value 0.333333
  ]
  edge [
    source 428
    target 429
    value 1
  ]
  edge [
    source 442
    target 443
    value 1
  ]
  edge [
    source 443
    target 738
    value 0.5
  ]
  edge [
    source 443
    target 675
    value 0.5
  ]
  edge [
    source 443
    target 676
    value 0.5
  ]
  edge [
    source 443
    target 739
    value 1
  ]
  edge [
    source 460
    target 461
    value 0.333333
  ]
  edge [
    source 460
    target 462
    value 0.333333
  ]
  edge [
    source 460
    target 463
    value 0.333333
  ]
  edge [
    source 461
    target 462
    value 0.333333
  ]
  edge [
    source 461
    target 463
    value 0.333333
  ]
  edge [
    source 462
    target 463
    value 0.333333
  ]
  edge [
    source 463
    target 638
    value 0.5
  ]
  edge [
    source 464
    target 466
    value 0.5
  ]
  edge [
    source 464
    target 465
    value 1.5
  ]
  edge [
    source 465
    target 466
    value 0.5
  ]
  edge [
    source 472
    target 984
    value 0.333333
  ]
  edge [
    source 472
    target 473
    value 0.833333
  ]
  edge [
    source 472
    target 474
    value 0.5
  ]
  edge [
    source 472
    target 1091
    value 0.333333
  ]
  edge [
    source 473
    target 1091
    value 0.333333
  ]
  edge [
    source 473
    target 1092
    value 0.833333
  ]
  edge [
    source 473
    target 984
    value 2.16667
  ]
  edge [
    source 473
    target 985
    value 0.333333
  ]
  edge [
    source 473
    target 474
    value 0.5
  ]
  edge [
    source 488
    target 489
    value 0.333333
  ]
  edge [
    source 490
    target 491
    value 0.5
  ]
  edge [
    source 490
    target 492
    value 1
  ]
  edge [
    source 490
    target 493
    value 0.5
  ]
  edge [
    source 500
    target 503
    value 1.5
  ]
  edge [
    source 500
    target 1221
    value 0.5
  ]
  edge [
    source 500
    target 502
    value 2.5
  ]
  edge [
    source 500
    target 501
    value 1
  ]
  edge [
    source 501
    target 502
    value 1
  ]
  edge [
    source 502
    target 503
    value 0.5
  ]
  edge [
    source 507
    target 508
    value 1.08333
  ]
  edge [
    source 507
    target 509
    value 0.75
  ]
  edge [
    source 508
    target 509
    value 0.75
  ]
  edge [
    source 514
    target 515
    value 0.833333
  ]
  edge [
    source 514
    target 516
    value 0.833333
  ]
  edge [
    source 514
    target 517
    value 0.333333
  ]
  edge [
    source 515
    target 516
    value 2.33333
  ]
  edge [
    source 515
    target 517
    value 0.333333
  ]
  edge [
    source 515
    target 674
    value 0.5
  ]
  edge [
    source 516
    target 1088
    value 1
  ]
  edge [
    source 516
    target 517
    value 2.91667
  ]
  edge [
    source 516
    target 1089
    value 0.5
  ]
  edge [
    source 516
    target 674
    value 0.5
  ]
  edge [
    source 516
    target 788
    value 0.333333
  ]
  edge [
    source 516
    target 1086
    value 0.5
  ]
  edge [
    source 516
    target 1087
    value 2.5
  ]
  edge [
    source 517
    target 963
    value 0.333333
  ]
  edge [
    source 517
    target 1460
    value 0.333333
  ]
  edge [
    source 517
    target 964
    value 0.333333
  ]
  edge [
    source 517
    target 1341
    value 1
  ]
  edge [
    source 546
    target 547
    value 1
  ]
  edge [
    source 547
    target 1389
    value 0.5
  ]
  edge [
    source 547
    target 1239
    value 1
  ]
  edge [
    source 548
    target 549
    value 0.333333
  ]
  edge [
    source 548
    target 550
    value 0.333333
  ]
  edge [
    source 549
    target 550
    value 0.333333
  ]
  edge [
    source 550
    target 1030
    value 0.5
  ]
  edge [
    source 561
    target 562
    value 0.458333
  ]
  edge [
    source 574
    target 576
    value 0.5
  ]
  edge [
    source 574
    target 575
    value 0.5
  ]
  edge [
    source 585
    target 586
    value 0.333333
  ]
  edge [
    source 585
    target 587
    value 0.333333
  ]
  edge [
    source 586
    target 587
    value 0.333333
  ]
  edge [
    source 587
    target 729
    value 0.5
  ]
  edge [
    source 589
    target 592
    value 0.333333
  ]
  edge [
    source 589
    target 1180
    value 0.25
  ]
  edge [
    source 589
    target 1181
    value 0.25
  ]
  edge [
    source 589
    target 590
    value 0.583333
  ]
  edge [
    source 589
    target 591
    value 0.583333
  ]
  edge [
    source 590
    target 591
    value 1.58333
  ]
  edge [
    source 590
    target 592
    value 0.333333
  ]
  edge [
    source 590
    target 1180
    value 0.25
  ]
  edge [
    source 590
    target 1181
    value 0.25
  ]
  edge [
    source 591
    target 592
    value 0.333333
  ]
  edge [
    source 591
    target 1180
    value 0.25
  ]
  edge [
    source 591
    target 1181
    value 0.25
  ]
  edge [
    source 609
    target 610
    value 0.5
  ]
  edge [
    source 609
    target 611
    value 0.333333
  ]
  edge [
    source 609
    target 612
    value 0.333333
  ]
  edge [
    source 611
    target 612
    value 0.333333
  ]
  edge [
    source 638
    target 640
    value 1
  ]
  edge [
    source 638
    target 639
    value 0.25
  ]
  edge [
    source 646
    target 853
    value 0.5
  ]
  edge [
    source 652
    target 653
    value 0.333333
  ]
  edge [
    source 652
    target 654
    value 2.08333
  ]
  edge [
    source 652
    target 655
    value 2.08333
  ]
  edge [
    source 652
    target 656
    value 0.333333
  ]
  edge [
    source 652
    target 657
    value 0.583333
  ]
  edge [
    source 652
    target 893
    value 0.333333
  ]
  edge [
    source 653
    target 654
    value 0.333333
  ]
  edge [
    source 653
    target 655
    value 0.333333
  ]
  edge [
    source 654
    target 864
    value 0.5
  ]
  edge [
    source 654
    target 865
    value 0.5
  ]
  edge [
    source 654
    target 774
    value 0.333333
  ]
  edge [
    source 654
    target 1130
    value 0.833333
  ]
  edge [
    source 654
    target 655
    value 2.08333
  ]
  edge [
    source 654
    target 656
    value 0.333333
  ]
  edge [
    source 654
    target 657
    value 0.916667
  ]
  edge [
    source 654
    target 893
    value 0.666667
  ]
  edge [
    source 654
    target 863
    value 0.5
  ]
  edge [
    source 655
    target 656
    value 0.333333
  ]
  edge [
    source 655
    target 657
    value 0.583333
  ]
  edge [
    source 655
    target 893
    value 0.333333
  ]
  edge [
    source 657
    target 774
    value 0.333333
  ]
  edge [
    source 657
    target 1130
    value 0.333333
  ]
  edge [
    source 675
    target 676
    value 0.5
  ]
  edge [
    source 676
    target 1556
    value 0.333333
  ]
  edge [
    source 676
    target 1558
    value 0.333333
  ]
  edge [
    source 676
    target 1557
    value 0.333333
  ]
  edge [
    source 693
    target 696
    value 0.2
  ]
  edge [
    source 693
    target 697
    value 1.2
  ]
  edge [
    source 693
    target 698
    value 0.2
  ]
  edge [
    source 693
    target 694
    value 0.2
  ]
  edge [
    source 693
    target 695
    value 0.2
  ]
  edge [
    source 694
    target 696
    value 0.2
  ]
  edge [
    source 694
    target 697
    value 0.2
  ]
  edge [
    source 694
    target 698
    value 0.2
  ]
  edge [
    source 694
    target 695
    value 0.2
  ]
  edge [
    source 695
    target 715
    value 0.25
  ]
  edge [
    source 695
    target 716
    value 0.25
  ]
  edge [
    source 695
    target 717
    value 0.25
  ]
  edge [
    source 695
    target 718
    value 0.25
  ]
  edge [
    source 695
    target 696
    value 0.2
  ]
  edge [
    source 695
    target 697
    value 0.2
  ]
  edge [
    source 695
    target 698
    value 0.2
  ]
  edge [
    source 696
    target 697
    value 0.2
  ]
  edge [
    source 696
    target 698
    value 0.2
  ]
  edge [
    source 697
    target 1394
    value 0.142857
  ]
  edge [
    source 697
    target 1395
    value 0.142857
  ]
  edge [
    source 697
    target 1396
    value 0.142857
  ]
  edge [
    source 697
    target 1145
    value 0.142857
  ]
  edge [
    source 697
    target 698
    value 0.2
  ]
  edge [
    source 697
    target 1397
    value 0.142857
  ]
  edge [
    source 700
    target 701
    value 0.333333
  ]
  edge [
    source 700
    target 702
    value 0.333333
  ]
  edge [
    source 701
    target 702
    value 0.333333
  ]
  edge [
    source 709
    target 710
    value 0.333333
  ]
  edge [
    source 715
    target 716
    value 0.25
  ]
  edge [
    source 715
    target 717
    value 0.25
  ]
  edge [
    source 715
    target 718
    value 0.25
  ]
  edge [
    source 716
    target 717
    value 0.25
  ]
  edge [
    source 716
    target 718
    value 0.25
  ]
  edge [
    source 717
    target 718
    value 0.25
  ]
  edge [
    source 736
    target 737
    value 0.5
  ]
  edge [
    source 756
    target 1123
    value 0.5
  ]
  edge [
    source 756
    target 775
    value 0.2
  ]
  edge [
    source 756
    target 764
    value 0.533333
  ]
  edge [
    source 756
    target 757
    value 0.5
  ]
  edge [
    source 756
    target 758
    value 0.5
  ]
  edge [
    source 756
    target 759
    value 1
  ]
  edge [
    source 756
    target 760
    value 1.5
  ]
  edge [
    source 756
    target 761
    value 1.86667
  ]
  edge [
    source 756
    target 762
    value 0.333333
  ]
  edge [
    source 756
    target 763
    value 0.333333
  ]
  edge [
    source 756
    target 892
    value 0.2
  ]
  edge [
    source 756
    target 765
    value 0.533333
  ]
  edge [
    source 757
    target 977
    value 0.25
  ]
  edge [
    source 757
    target 758
    value 1.25
  ]
  edge [
    source 758
    target 977
    value 0.25
  ]
  edge [
    source 761
    target 774
    value 1.33333
  ]
  edge [
    source 761
    target 775
    value 1.53333
  ]
  edge [
    source 761
    target 776
    value 0.333333
  ]
  edge [
    source 761
    target 764
    value 0.533333
  ]
  edge [
    source 761
    target 762
    value 0.666667
  ]
  edge [
    source 761
    target 763
    value 0.666667
  ]
  edge [
    source 761
    target 892
    value 0.2
  ]
  edge [
    source 761
    target 765
    value 0.533333
  ]
  edge [
    source 762
    target 763
    value 0.666667
  ]
  edge [
    source 764
    target 1255
    value 0.833333
  ]
  edge [
    source 764
    target 775
    value 0.2
  ]
  edge [
    source 764
    target 892
    value 0.2
  ]
  edge [
    source 764
    target 765
    value 0.866667
  ]
  edge [
    source 765
    target 1255
    value 0.333333
  ]
  edge [
    source 765
    target 775
    value 0.2
  ]
  edge [
    source 765
    target 892
    value 0.2
  ]
  edge [
    source 770
    target 771
    value 0.2
  ]
  edge [
    source 770
    target 772
    value 0.2
  ]
  edge [
    source 770
    target 773
    value 0.2
  ]
  edge [
    source 771
    target 772
    value 0.2
  ]
  edge [
    source 771
    target 773
    value 0.2
  ]
  edge [
    source 772
    target 773
    value 0.2
  ]
  edge [
    source 774
    target 775
    value 1.33333
  ]
  edge [
    source 774
    target 776
    value 0.333333
  ]
  edge [
    source 774
    target 1130
    value 0.333333
  ]
  edge [
    source 775
    target 776
    value 0.333333
  ]
  edge [
    source 775
    target 892
    value 0.2
  ]
  edge [
    source 840
    target 1190
    value 0.2
  ]
  edge [
    source 840
    target 1191
    value 0.2
  ]
  edge [
    source 863
    target 864
    value 0.5
  ]
  edge [
    source 866
    target 867
    value 0.5
  ]
  edge [
    source 944
    target 945
    value 0.2
  ]
  edge [
    source 955
    target 956
    value 0.5
  ]
  edge [
    source 956
    target 1135
    value 0.583333
  ]
  edge [
    source 956
    target 1136
    value 0.25
  ]
  edge [
    source 956
    target 1137
    value 0.25
  ]
  edge [
    source 956
    target 1138
    value 0.333333
  ]
  edge [
    source 963
    target 964
    value 0.333333
  ]
  edge [
    source 983
    target 984
    value 1
  ]
  edge [
    source 983
    target 985
    value 0.5
  ]
  edge [
    source 983
    target 986
    value 0.5
  ]
  edge [
    source 984
    target 1091
    value 0.333333
  ]
  edge [
    source 984
    target 1092
    value 0.833333
  ]
  edge [
    source 984
    target 985
    value 0.833333
  ]
  edge [
    source 984
    target 986
    value 0.5
  ]
  edge [
    source 985
    target 1092
    value 0.333333
  ]
  edge [
    source 1021
    target 1022
    value 0.25
  ]
  edge [
    source 1021
    target 1023
    value 0.75
  ]
  edge [
    source 1022
    target 1023
    value 0.25
  ]
  edge [
    source 1024
    target 1025
    value 0.5
  ]
  edge [
    source 1026
    target 1416
    value 0.333333
  ]
  edge [
    source 1026
    target 1417
    value 0.333333
  ]
  edge [
    source 1026
    target 1027
    value 1
  ]
  edge [
    source 1039
    target 1040
    value 0.5
  ]
  edge [
    source 1082
    target 1083
    value 0.5
  ]
  edge [
    source 1086
    target 1087
    value 0.5
  ]
  edge [
    source 1087
    target 1088
    value 0.5
  ]
  edge [
    source 1087
    target 1089
    value 0.5
  ]
  edge [
    source 1121
    target 1122
    value 0.5
  ]
  edge [
    source 1135
    target 1136
    value 0.25
  ]
  edge [
    source 1135
    target 1137
    value 0.25
  ]
  edge [
    source 1135
    target 1138
    value 0.333333
  ]
  edge [
    source 1136
    target 1137
    value 0.25
  ]
  edge [
    source 1145
    target 1282
    value 0.333333
  ]
  edge [
    source 1145
    target 1283
    value 0.333333
  ]
  edge [
    source 1145
    target 1394
    value 0.142857
  ]
  edge [
    source 1145
    target 1395
    value 0.142857
  ]
  edge [
    source 1145
    target 1396
    value 0.142857
  ]
  edge [
    source 1145
    target 1397
    value 0.142857
  ]
  edge [
    source 1145
    target 1560
    value 0.333333
  ]
  edge [
    source 1145
    target 1561
    value 0.333333
  ]
  edge [
    source 1162
    target 1413
    value 0.25
  ]
  edge [
    source 1162
    target 1414
    value 0.25
  ]
  edge [
    source 1162
    target 1415
    value 0.25
  ]
  edge [
    source 1162
    target 1163
    value 0.5
  ]
  edge [
    source 1178
    target 1342
    value 0.333333
  ]
  edge [
    source 1180
    target 1181
    value 0.25
  ]
  edge [
    source 1190
    target 1191
    value 0.2
  ]
  edge [
    source 1195
    target 1196
    value 0.333333
  ]
  edge [
    source 1195
    target 1197
    value 0.333333
  ]
  edge [
    source 1196
    target 1197
    value 0.333333
  ]
  edge [
    source 1214
    target 1216
    value 0.25
  ]
  edge [
    source 1214
    target 1217
    value 0.25
  ]
  edge [
    source 1214
    target 1215
    value 0.25
  ]
  edge [
    source 1215
    target 1216
    value 0.25
  ]
  edge [
    source 1215
    target 1217
    value 0.25
  ]
  edge [
    source 1216
    target 1217
    value 0.25
  ]
  edge [
    source 1228
    target 1229
    value 0.25
  ]
  edge [
    source 1282
    target 1283
    value 0.333333
  ]
  edge [
    source 1312
    target 1313
    value 0.25
  ]
  edge [
    source 1312
    target 1314
    value 0.25
  ]
  edge [
    source 1312
    target 1315
    value 0.25
  ]
  edge [
    source 1312
    target 1316
    value 0.25
  ]
  edge [
    source 1313
    target 1314
    value 0.25
  ]
  edge [
    source 1313
    target 1315
    value 0.25
  ]
  edge [
    source 1313
    target 1316
    value 0.25
  ]
  edge [
    source 1314
    target 1315
    value 0.25
  ]
  edge [
    source 1314
    target 1316
    value 0.25
  ]
  edge [
    source 1315
    target 1316
    value 0.25
  ]
  edge [
    source 1315
    target 1468
    value 0.25
  ]
  edge [
    source 1315
    target 1469
    value 0.25
  ]
  edge [
    source 1315
    target 1470
    value 0.25
  ]
  edge [
    source 1343
    target 1344
    value 0.5
  ]
  edge [
    source 1347
    target 1348
    value 0.5
  ]
  edge [
    source 1361
    target 1362
    value 0.333333
  ]
  edge [
    source 1384
    target 1385
    value 0.5
  ]
  edge [
    source 1394
    target 1395
    value 0.142857
  ]
  edge [
    source 1394
    target 1396
    value 0.142857
  ]
  edge [
    source 1394
    target 1397
    value 0.142857
  ]
  edge [
    source 1395
    target 1396
    value 0.142857
  ]
  edge [
    source 1395
    target 1397
    value 0.142857
  ]
  edge [
    source 1396
    target 1397
    value 0.142857
  ]
  edge [
    source 1404
    target 1408
    value 0.166667
  ]
  edge [
    source 1404
    target 1405
    value 0.166667
  ]
  edge [
    source 1404
    target 1406
    value 0.166667
  ]
  edge [
    source 1404
    target 1407
    value 0.166667
  ]
  edge [
    source 1405
    target 1408
    value 0.166667
  ]
  edge [
    source 1405
    target 1406
    value 0.166667
  ]
  edge [
    source 1405
    target 1407
    value 0.166667
  ]
  edge [
    source 1406
    target 1408
    value 0.166667
  ]
  edge [
    source 1406
    target 1407
    value 0.166667
  ]
  edge [
    source 1407
    target 1408
    value 0.166667
  ]
  edge [
    source 1413
    target 1414
    value 0.25
  ]
  edge [
    source 1413
    target 1415
    value 0.25
  ]
  edge [
    source 1414
    target 1415
    value 0.25
  ]
  edge [
    source 1416
    target 1417
    value 0.333333
  ]
  edge [
    source 1468
    target 1470
    value 0.25
  ]
  edge [
    source 1468
    target 1469
    value 0.25
  ]
  edge [
    source 1469
    target 1470
    value 0.25
  ]
  edge [
    source 1481
    target 1482
    value 0.5
  ]
  edge [
    source 1550
    target 1551
    value 0.333333
  ]
  edge [
    source 1556
    target 1557
    value 0.333333
  ]
  edge [
    source 1556
    target 1558
    value 0.333333
  ]
  edge [
    source 1557
    target 1558
    value 0.333333
  ]
  edge [
    source 1560
    target 1561
    value 0.333333
  ]
]
