#include "loglikelihoodconvergence.h"
#include <cmath>
#include <vector>
#include <iostream>
namespace{
  float EPS = 1.0E-18;
  bool kDEBUG = false;
}

typedef unsigned int uint;

bool LogLikelihoodConvergence::isConverged()
{
    //first iteration
    if (previous_measure_ == 0.){
        return false;
    }
    if (fabs(previous_measure_ - current_measure_)/fabs(current_measure_) < tolerance_){
        return true;
    }
    else {
        return false;
    }
}

void LogLikelihoodConvergence::DumpIteration(float const * const * const k_iz,
                                             uint const * const * const A_indeces,
                                             uint nE,
                                             uint nV, uint nK)
{
    previous_measure_ = current_measure_;
    double loglikelihood=0.0;
    double sum_t; // variable to hold temporary values
    std::vector<double> kz(nK, 0.0);
    for (uint z=0; z<nK; ++z){
        kz[z] = 0.0;
        for (uint i=0; i<nV; ++i){
            kz[z] += k_iz[z][i];
          }
    }
    for (uint link = 0; link < nE; ++link)
    {
        sum_t = 0.0;
        uint i = A_indeces[link][0];
        uint j = A_indeces[link][1];
        for (uint z=0; z<nK; z++)
        {
            sum_t += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
        }
        loglikelihood += 2*log(sum_t + EPS);
    }
    for (uint i=0; i < nV; ++i){
        for (uint j=0; j < nV; ++j){
            sum_t = 0.0;
            for (uint z=0;z<nK;z++)
            {
                sum_t += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
            }
            loglikelihood -= sum_t;
        }
    }
    current_measure_ = loglikelihood;
    if (kDEBUG){
        std::cout.precision(16);
        std::cout << loglikelihood << std::endl;
    }
}
