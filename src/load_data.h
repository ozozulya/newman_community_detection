#include <string>
#include <map>
#include <utility> // pair

// Functions for loading data from files.
#ifndef LOAD_DATA_H
#define LOAD_DATA_H

typedef unsigned int uint;

void load_data(const std::string& fname,
                   std::map<std::string, float>& link_weights,
                   std::map<std::string, std::pair<uint, uint> >& link_edges);

#endif // LOAD_DATA_H
