#include "load_data.h"
#include <iostream>
#include <iterator>
#include <time.h>
#include <math.h>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <numeric>
#include <algorithm>
#include <assert.h>
#include "utils.h"
#include "readgml/readgml.h"
static const bool kDEBUG = false;

using namespace std;

//read the graph data from the file into the float** array
static void load_txt_data(const string& fname,
               map<string, float>& link_weights,
               map<string, pair<uint, uint> >& link_edges){
    // TODO: map probably has to be changed to hash_map?
    // Think later
    link_weights.clear();
    link_edges.clear();
    ifstream finput(fname.c_str());
    if (!finput) {
            cerr << "The file " << fname << " cannot be opened.\n";
            abort(); // TODO: or better throw
    } else {
        cout<< "Opened file: " << fname << endl;
    }
    string line;
    vector<string> tokens;
    while(getline(finput, line)){
        string::size_type indx = line.rfind("#");
        if(indx != string::npos)
            line.erase(indx);
        tokens.clear();
        if (!line.empty()){
             istringstream iss(line);
             std::copy(istream_iterator<string>(iss), istream_iterator<string>(),
                                        back_inserter<vector<string> >(tokens));
             if (!(tokens.size() == 3) && !(tokens.size() == 0)){
                 cerr << "Wrong data format. Number of tokens: " << tokens.size() << endl;
             }
             else{
                 //num_edges[atoi(tokens[0].c_str())][atoi(tokens[1].c_str())] = atof(tokens[2].c_str());
                 link_weights[tokens[0]+' '+tokens[1]] = atof(tokens[2].c_str());
                 link_edges[tokens[0]+' '+tokens[1]] = make_pair(atoi(tokens[0].c_str()),
                                                                 atoi(tokens[1].c_str()));
             }
        }
    }

    // print the graph
//        map<string, float>::const_iterator it = link_weights.begin();
//        for (;it != link_weights.end(); ++it){
//            cout << it->first << " : " << it->second << endl;
//            cout << it->first << " : "
//                    << link_edges[it->first].first << ' '
//                    << link_edges[it->first].second << endl;
//        }
}

// a wrapper function to read data from gml format
static void load_gml_data(const string& fname,
                   map<string, float>& link_weights,
                   map<string, pair<uint, uint> >& link_edges){
    NETWORK* network = new NETWORK; //TODO: C++11 use scoped ptr
    FILE* fileptr;
    fileptr = fopen(fname.c_str(), "r");
    if (fileptr == NULL){
        cerr << "Couldn't find the file\n";
        return;
    }
    int res = read_network(network, fileptr);
    if (res == 0){
        cout << "The file has been succesefully read\n"
             << "The number of vertices in the graph is: " << network->nvertices
             << endl;
        link_weights.clear();
        link_edges.clear();
        set<string> keys;
        for (size_t v=0; v<network->nvertices; ++v){
            for (size_t e=0; e < network->vertices[v].edges.size(); ++e){
                string key;
                key = ConvertToString<size_t>(v)+" "+ ConvertToString<size_t>(network->vertices[v].edges[e].target);
                // check for repeated edges and self-links
                if (keys.find(key) != keys.end()){
                    cout << "Repeated edge: " << key << endl;
                    cout << network->vertices[v].label << endl;
                    continue;
                }
                if (v == network->vertices[v].edges[e].target){
                    cout << "Self-link!\n" << v << endl;
                    continue;
                }
                keys.insert(key);
                link_weights.insert(pair<string, float>(key, network->vertices[v].edges[e].weight));
                link_edges.insert(make_pair(key, make_pair(v, network->vertices[v].edges[e].target)));
            }
        }
        assert(link_weights.size()==link_edges.size());
        cout << "Number of edges/links: " << link_edges.size() << endl;
    }
    else
        cerr << "Reading of the graph has failed.\n";
    delete network;
    fclose(fileptr);
}


void load_data(const string& fname,
               map<string, float>& link_weights,
               map<string, pair<uint, uint> >& link_edges){
    if ((fname.rfind(".gml") != string::npos))
        load_gml_data(fname, link_weights, link_edges);
    else
        load_txt_data(fname, link_weights, link_edges);
};

