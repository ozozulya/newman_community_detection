#include "convergence_helper.h"
#include "loglikelihoodconvergence.h"
#include "scalardiff_convergence.h"

namespace{
    ConvergenceHelper<LogLikelihoodConvergence> Register1("loglikelihood_convergence");
    ConvergenceHelper<ScalarDiffConvergence> Register2("scalardiff_convergence");
}
