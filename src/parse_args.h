#ifndef PARSE_ARGS_H
#define PARSE_ARGS_H

struct Parameters;

// Parse the command line options and initialize the parameters of the algorithm.
bool parseArguments(int argc, char **argv, Parameters &params);

#endif // PARSE_ARGS_H
