#include "testgraph.h"
#include <cmath>
#include "park_miller.h"
// TODO: move the concrete synthetic graph
// into separate files

namespace{

const bool kDEBUG = false;
inline void add(link_vertices_t& link_vertices, uint i, uint j)
{//TODO: change to emplace C++11
    int a, b;
    if (i < j) {a = i; b = j;} else {a = j; b = i;}
    link_vertices.insert(std::pair<std::string, std::pair<uint, uint> >
                         (ConvertToString(a)+" "+ConvertToString(b),
                                   std::make_pair(a, b)));
    return;
}

inline void add(link_weights_t& link_weights, uint i, uint j, float w)
{//TODO: change to emplace C++11
    int a, b;
    if (i < j) {a = i; b = j;} else {a = j; b = i;}
    link_weights.insert(std::make_pair(ConvertToString<uint>(a)+
                                       " "+ConvertToString<uint>(b),
                                   w));
    return;
}

/*
//draw k random elements from interval [a, b] (inclusive boundaries)
//TODO add dependence on RandomNumber generator (Boost or C++11), should return ptrdiff_t
std::set<uint> random_sample(uint a, uint b, uint k, const std::vector<bool>& full){
    std::vector<uint> values;
    std::set<uint> samples;
    if (kDEBUG)
        std::cerr << "Start random sampling.\n";
    if (k == 0) return samples;
    values.reserve(b-a+1);
    for(uint i = a; i <= b; ++i)
        if (!full[i])
        values.push_back(i);
    if (values.empty()){
        if (kDEBUG)
            std::cerr << "No values to be sampled\n";
        return samples;
    }
    if (values.size() <= k){ //return all of them
        std::copy(values.begin(), values.end(),
                  std::inserter(samples, samples.begin()));
        return samples;
    }
    std::iterator_traits<std::vector<uint>::iterator>::difference_type i, n;
    std::vector<uint>::iterator first = values.begin();
    n = (values.end() - values.begin());
    //this shuffles k sample at the end of the vector similar to std::random_shuffle
    for(i = n - 1; i > n-k-1; --i) std::swap(first[i], first[rand()%(i+1)]);
    std::vector<uint>::reverse_iterator it = values.rbegin();
    for( uint i = k; i > 0; ++it, --i)
    {
        samples.insert(*it);
    }
    if (kDEBUG){
        std::cerr << "From interval [" << a << "," << b << "]"
                  << " the following number have been chosen\n";
        std::copy(samples.begin(), samples.end(),
              std::ostream_iterator<uint>(std::cerr, " "));
        std::cerr << std::endl;
    }
    return samples;
}

*/

class ParkMillerWrapper{
     ParkMiller gen;
public:
     ParkMillerWrapper(uint seed=17){
         gen=ParkMiller(seed);
     }
     // generate one number below i
    int operator()(int i){
        return gen.GetOneRandomInteger()%i;
    }
};

template<class RandomNumberGenerator>
std::set<uint> random_sample(uint a, uint b, uint k, const std::vector<bool>& full,
                    RandomNumberGenerator& generator){
    std::vector<uint> values;
    std::set<uint> samples;
    if (kDEBUG)
        std::cerr << "Start random sampling.\n";
    if (k == 0) return samples;
    values.reserve(b-a+1);
    for(uint i = a; i <= b; ++i)
        if (!full[i])
        values.push_back(i);
    if (values.empty()){
        if (kDEBUG)
            std::cerr << "No values to be sampled\n";
        return samples;
    }
    if (values.size() <= k){ //return all of them
        std::copy(values.begin(), values.end(),
                  std::inserter(samples, samples.begin()));
        return samples;
    }
    std::iterator_traits<std::vector<uint>::iterator>::difference_type i, n;
    std::vector<uint>::iterator first = values.begin();
    n = (values.end() - values.begin());
    //this shuffles k sample at the end of the vector similar to std::random_shuffle
    for(i = n - 1; i > n-k-1; --i) std::swap(first[i], first[generator(i+1)]);
    std::vector<uint>::reverse_iterator it = values.rbegin();
    for( uint i = k; i > 0; ++it, --i)
    {
        samples.insert(*it);
    }
    if (kDEBUG){
        std::cerr << "From interval [" << a << "," << b << "]"
                  << " the following numbers have been chosen\n";
        std::copy(samples.begin(), samples.end(),
              std::ostream_iterator<uint>(std::cerr, " "));
        std::cerr << std::endl;
    }
    return samples;
}

int myrandom (int i) { return std::rand()%i;}



}//end anonymous namespace

TestGraphBridge::TestGraphBridge(const TestGraphInner& original)
{
    TestGraphObj_ = original.clone();
}
TestGraphBridge::TestGraphBridge(const TestGraphBridge& original)
{
    TestGraphObj_ = original.TestGraphObj_->clone();
}

TestGraphBridge::~TestGraphBridge() { delete TestGraphObj_;}

TestGraphBridge& TestGraphBridge::operator=(const TestGraphBridge& original) {
    if (this != &original){
        delete TestGraphObj_;
        TestGraphObj_ = original.TestGraphObj_->clone();
    }
    return *this;
}

link_weights_t TestGraphSynthetic::get_link_weights() const
    {return link_weights_;}
link_vertices_t TestGraphSynthetic::get_link_edges() const
    {return link_vertices_;}

// FIXME: this should be removed.
TestGraphSynthetic::TestGraphSynthetic()
{
    nV_=nE_=nK_=nA_=nB_=0;
    link_weights_.clear();
    link_vertices_.clear();
    vertix_communities_.clear();
}


/**
  This generates a graph with 2 overlapping comminities. The community A
  has nA vertices intraconnected with k links each, the same is for the
  community B with nB vertices. The rest of the links belongs to the mixed
  community, where each vertex interconnceted with k/2 links to random
  vertices in a community A, and k/2 links to the community B.
  */

bool TestGraphSynthetic::GenerateGraph(uint nV, //total number of vertices
                                  uint nA, //number of vertices in the A community
                                  uint nB, //number of vertices in the B community
                                  uint k) // degree of the Vertex
{


    if (!CheckParamsConsistency(nV, nA, nB, k)){
        return false;
    }
    nV_=nV;
    nK_=2;
    nA_=nA;
    nB_=nB;
    ParkMillerWrapper park_miller;
    // The number of links that have been already created for a given vertex.
    // By the end of the initialization this should be k.
    std::vector<uint> vertex_N_links(nV_,0);
    //the infamous vector<bool> to check if the vertex has already k links.
    std::vector<bool> vertex_full(nV_, false);
    std::vector<std::set<uint> > vertex_links(nV_);
    // the total number of edges should be as calculate below, but for small
    // graphs this is not necessary so.
    nE_ = k*nV_/2;
    std::set<uint> new_links, nlA, nlB;
    //first fill in the overlapping block
    if (kDEBUG)
        std::cerr << "Generating graph links AB\n";
    for(uint i=nA+nB; i < nV; ++i){
        // must not happen as we fill in links sequentually
        // to communities A and B
        assert((vertex_full[i]) == false);
        vertex_full[i] = true;
        new_links.clear();
        nlA = random_sample<ParkMillerWrapper>(0, nA-1, k/2, vertex_full, park_miller); // links to community A
        nlB = random_sample(nA, nA+nB-1, k/2, vertex_full, park_miller);//links to community B
        std::set_union(nlA.begin(), nlA.end(), nlB.begin(), nlB.end(),
                       std::inserter(new_links, new_links.begin()));
        assert(new_links.size() == k); // make sure we made k new links
        //TODO: change for in C++11
        for(std::set<uint>::const_iterator it = new_links.begin();
            it != new_links.end(); ++it){
            vertex_links[*it].insert(i);
            vertex_links[i].insert(*it);
            vertex_N_links[i] += 1;
            vertex_N_links[*it] += 1;
            // update the vector with completely fill vertices
            if (vertex_N_links[i]==k) vertex_full[i] = true;
            if (vertex_N_links[*it]==k) vertex_full[*it] = true;
            assert( i != *it); //TODO: throw
            add(link_vertices_, i, *it);
            add(link_weights_, i, *it, 1.0);
        };
    }

    if (kDEBUG)
        std::cerr << "Generating graph links A\n";
    //create random links within block A
    for(uint i=0; i<nA; ++i){
        //skip if the vertex already has k links, which could happen by chance.
        if (vertex_full[i]) continue;
        if (vertex_N_links[i] == k) {
            vertex_full[i]=true;
            continue;
        }
        vertex_full[i] = true;
        new_links = random_sample(0, nA-1, k-vertex_N_links[i],vertex_full,
                                  park_miller);
        for(std::set<uint>::const_iterator it = new_links.begin();
            it != new_links.end(); ++it){
            vertex_links[*it].insert(i);
            vertex_links[i].insert(*it);
            vertex_N_links[i] += 1;
            vertex_N_links[*it] += 1;
            if (vertex_N_links[i]==k) vertex_full[i] = true;
            if (vertex_N_links[*it]==k) vertex_full[*it] = true;
            assert( i != *it); //TODO: throw
            add(link_vertices_, i, *it);
            add(link_weights_, i, *it, 1.0);
        };

    }
    if (kDEBUG)
        std::cerr << "Generating graph links B\n";
    //create random links within block B
    for(uint i=nA; i<nA+nB-1; ++i){
        //skip if the vertex already has k links, which could happen by chance.
        if (vertex_full[i]) continue;
        if (vertex_N_links[i] == k) {
            vertex_full[i]=true;
            continue;
        }
        vertex_full[i] = true;
        new_links = random_sample(nA, nA+nB-1, k-vertex_N_links[i], vertex_full,
                                  park_miller);
        for(std::set<uint>::const_iterator it = new_links.begin();
            it != new_links.end(); ++it){
            vertex_links[*it].insert(i);
            vertex_links[i].insert(*it);
            vertex_N_links[i] +=1;
            vertex_N_links[*it] +=1;
            if (vertex_N_links[i]==k) vertex_full[i] = true;
            if (vertex_N_links[*it]==k) vertex_full[*it] = true;
            assert(i != *it); //TODO: throw
            add(link_vertices_, i, *it);
            add(link_weights_, i, *it, 1.0);
        };

    }

    // create the vector with the result.
    //TODO: change name vertix to vertex
    vertix_communities_.clear();
    for (uint i = 0; i < nV_; ++i){
        std::vector<float> vA(2, 0.F), vB(2, 0.F), vAB(2, 0.F);
        vA[0]=1.F; vB[1]=1.F; vAB[0] = 0.5; vAB[1] = 0.5;
        if (i < nA){ vertix_communities_[i] = vA;}
        else if ( i >= nA && i < nB){ vertix_communities_[i] =vB;}
        else {vertix_communities_[i] = vAB;}
    }

    //update the number of links (edges).
    nE_ = link_vertices_.size();
    return true; //the graph has been generated
}

/* Make a sanity check on the passed parameters
TODO: more checks could be done? */
bool TestGraphSynthetic::CheckParamsConsistency(uint nV, uint nA,
                                                  uint nB, uint k)
{
    return (nA+nB < nV) && (k % 2 == 0) && (nA > 0) && (nB > 0);
}

/**
 * @brief TestGraphSynthetic::test_result check if the result of
 * the community finding algorithms coincides with the 2 known communities.
 * @param result
 * @return
 */
double TestGraphSynthetic::TestResult(const std::vector<std::vector<float> >& result) const{
    // We know that the first nA nodes should belong to one community,
    // the next nB nodes to another, and the rest is mixed in the proportion 50/50%

    // ideally result[i] in this loop should look like {0., 1.0} or {1.0, 0}
    // as we don't know where the community will fall. I keep track of this ambiguity
    // with enum
    enum FirstOrSecond{ First=0, Second=1};
    FirstOrSecond Aindex, Bindex; // which community correspond to which index
    double first(0.), second(0.);
    double deviation(0.0); // deviation from the true community assignement.
    for(uint i = 0; i < nA_; ++i){
        first +=result[0][i];
        second += result[1][i];
    }
    Aindex = first>second ? First : Second;
    deviation += static_cast<double>(nA_)- std::max(first, second);

    first=second=0.0;
    for(uint i = nA_; i < nA_+nB_; ++i){
        first +=result[0][i];
        second += result[1][i];
    }
    Bindex = first>second ? First : Second;
    // the indeces for communities should be different.
    if (Aindex == Bindex) {
        return 1.0;
    }
    deviation += static_cast<double>(nB_) - std::max(first, second);

    // the rest of the vertices should be in the mixed community
    for(uint i = nA_+nB_; i < nV_; ++i){
        deviation += std::fabs(result[0][i]-0.5) + std::fabs(result[1][i]-0.5);
    }
    std::cout << "Deviation: " << deviation << std::endl;
    return deviation/nV_;
}

// TODO: remove this static memeber
// Tolerance: if one randomly guesses between 1 and 0, when the answer is 0
// or 1 the average error is 0.5. If the correct answer is 0.5, the error is
// 0.25*2=0.5 as well.
const double TestGraphSynthetic::tolerance_ = 0.2;

/** print the generated graph for small sizes for debuging
  purposes
  */
void TestGraphSynthetic::PrintGraph(uint max_display_graph) {
    if (nV_ == 0){
        std::cerr << "You have to generate graph first\n";
        return;
    }

    if (nV_ > max_display_graph){
        std::cerr << "The graph is to large to be shown,\
                     number of vertices: "
                  << nV_
                  << "\n The graph with max " <<max_display_graph
                  << " can be shown.\n";
        return;
    }

    std::cout << "Number of links   : " <<  nE_ << std::endl
              << "Number of vertices: " << nV_ << std::endl;
    /*for(link_vertices_t::const_iterator it = link_vertices_.begin(); it != link_vertices_.end(); ++it){
        std::cout << it->first << std::endl;
    }
    */
    CreateAdjacencyList();
    for(uint i=0; i<nV_; ++i){
        std::cout << i << " :";
        std::copy(adjacency_list_[i].begin(), adjacency_list_[i].end(),
                  std::ostream_iterator<uint>(std::cout, " "));
        std::cout << std::endl;
    }

   return;
}

TestGraphInner* TestGraphSynthetic::clone() const{
    return new TestGraphSynthetic(*this); // use default copy constructor
}

/**
 * @brief TestGraphSynthetic::CreateAdjacencyList
 *Create adjacency list from edges and attached vertices.
 */
void TestGraphSynthetic::CreateAdjacencyList(){
    adjacency_list_.resize(nV_);
    for(link_vertices_t::const_iterator it = link_vertices_.begin(); it != link_vertices_.end(); ++it){
        adjacency_list_[it->second.first].push_back(it->second.second);
        adjacency_list_[it->second.second].push_back(it->second.first);
    }
    // now sort them for better representation
    for(uint i=0; i<nV_; ++i){
        adjacency_list_[i].sort();
    }
}

