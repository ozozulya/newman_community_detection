#ifndef PARK_MILLER_H
#define PARK_MILLER_H

// http://en.wikipedia.org/wiki/Lehmer_random_number_generator
// This is copied from Mark Joshi's Financial Derivatives Pricing

class ParkMiller
{
public:
    ParkMiller(long seed = 1);
    long GetOneRandomInteger();
    void SetSeed(long seed);
    static unsigned long Max();
    static unsigned long Min();
private:
    long seed_;
};

#endif // PARK_MILLER_H
