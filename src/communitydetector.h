#ifndef COMMUNITYDETECTOR_H
#define COMMUNITYDETECTOR_H
#include <map>
#include <vector>
#include "utils.h"
#include "parameters.h"
#include <sstream>
#include <iostream>

class RandomInit;

class CommunityDetector
{
    typedef std::map<std::string, float> link_weights_t;
    typedef std::map<std::string, std::pair<uint, uint> > link_vertices_t;
public:
    CommunityDetector(
        const AlgorithmParameters& params,
        const link_weights_t& link_weights,
        const link_vertices_t& link_edges,
        const RandomInit& initializer); // rand init class
    void run();
    std::vector<std::vector<float> > get_result() const
        {return k_win_;}
    std::vector<float> get_likelihoods() const
        {return likelihoods_;}

private:
    const RandomInit&  initializer_; // TODO: external dependencies
    AlgorithmParameters params_;
    std::vector<float> likelihoods_;
    std::vector<std::vector<float> > k_win_;
    link_weights_t link_weights_;
    link_vertices_t link_edges_;
    CommunityDetector();
    DISALLOW_COPY_AND_ASSIGN(CommunityDetector)

};

#endif // COMMUNITYDETECTOR_H
