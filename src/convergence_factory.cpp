#include "convergence_factory.h"
#include "convergence_measure.h"
using namespace std;

void ConvergenceFactory::RegisterConvergence(
        const string &ConvergenceId,
        CreateConvergenceFunction CreatorFunction)
{
    CreatorFunctions.insert(
                pair<string, CreateConvergenceFunction>(
                    ConvergenceId, CreatorFunction));
}

ConvergenceMeasure* ConvergenceFactory::CreateConvergence(string ConvergenceId, double tolerance){
    map<string, CreateConvergenceFunction>::const_iterator
            i = CreatorFunctions.find(ConvergenceId);
    if (i == CreatorFunctions.end()){
        return NULL;
    }
    return (i->second)(tolerance);
}

ConvergenceFactory& ConvergenceFactory::Instance(){
    static ConvergenceFactory theFactory;
    return theFactory;
}
