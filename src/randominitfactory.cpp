#include "randominitfactory.h"
#include <iostream> //FIXME: why do we need it?
#include "randominit.h"

using namespace std;


void RandomInitFactory::RegisterRandomInit(
        string RandInitId,
        CreateRandomInitFunction CreatorFunction)
{
    CreatorFunctions.insert(
        pair<string, CreateRandomInitFunction>(
            RandInitId, CreatorFunction));
}

RandomInit* RandomInitFactory::CreateRandomInitializer(string RandInitId){
    map<string, CreateRandomInitFunction>::const_iterator
            i = CreatorFunctions.find(RandInitId);
    if (i == CreatorFunctions.end()){
        // cout << RandInitId << "Uknown random initializer\n";
        return NULL; // С++11: nullptr
    }
    return i->second();
}

RandomInitFactory& RandomInitFactory::Instance(){
    static RandomInitFactory theFactory;
    return theFactory;
}
