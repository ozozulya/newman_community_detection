#ifndef CONVERGENCE_MEASURE_H
#define CONVERGENCE_MEASURE_H

class ConvergenceMeasure
{
public:
    virtual void DumpIteration(float const * const * const,
                               unsigned int const * const * const,
                               unsigned int, unsigned int,
                               unsigned int) = 0;
    virtual bool isConverged() = 0;
    virtual ~ConvergenceMeasure(){}
};

#endif // CONVERGENCE_MEASURE_H
