#include "EM_core.h"
#include <iostream>
#include <vector>
#include "parameters.h"
#include "utils.h"
#include <cmath>
#include <vector>
#include <cassert>
#include <algorithm>
#include "convergence_factory.h"
#include "convergence_measure.h"
#include <memory>


namespace{

// a small number that is used for the
// regularization of the denominators and logarithms
const float EPS = 0.f;//1.0E-18;

bool kDEBUG = false; // debug local file

// the last entry is to track the number of items in the enumeration
enum PRUNNING_TYPE : uint8_t {
    SKIP_LINK,
    LINK_CONVERGED,
    SKIP_ZERO_COLOR,
    VALUE_BELOW_THRESHOLD,
    VERTEX_CONVERGED,
    SKIP_VERTEX,
    PRUNNING_TYPE_MAX // should be last
};

// Gather statistics about prunnings.
class StatisticsGatherer {
private:
    uint iteration_; // current iteration in the loop
    uint nIterations_; // max number of itereations
    std::vector< std::vector<uint> > prunnings_;
    std::unique_ptr<ConvergenceMeasure> pConvergenceMeasure_;

public:
    void DumpPrunning(uint iteration, PRUNNING_TYPE prun_type){
        prunnings_[iteration-1][prun_type]+=1;
    }

    StatisticsGatherer(uint nIterations, std::string ConvergenceId="",
                       double tolerance = 0.) {
        iteration_ = 0;
        pConvergenceMeasure_ = std::unique_ptr<ConvergenceMeasure>{
                ConvergenceFactory::Instance().CreateConvergence(ConvergenceId, tolerance)
        };

        prunnings_ = std::vector<std::vector<uint>>(nIterations);
        for(uint i = 0; i < nIterations; ++i ){
            prunnings_[i].resize(PRUNNING_TYPE_MAX, 0);
        }
    }

    void PrintPrunnings(std::ostream& os = std::cout){
        for(uint i =0; i < prunnings_.size(); ++i ){
            os << std::endl << "Iteration " << i << " : " ;
            for(uint j=0; j < PRUNNING_TYPE_MAX; ++j){
                os << " " << prunnings_[i][j];
            }
        }
        os << std::endl;
    }

    /* Gather statistics about iterations
     *
     */
    void DumpIteration(float const * const * const k_iz,
                       uint const * const * const A_indeces,
                       uint nE,
                       uint nV, uint nK){
        iteration_ += 1;
        if (pConvergenceMeasure_ != NULL)
            pConvergenceMeasure_->DumpIteration(k_iz, A_indeces,
                                        nE, nV, nK);
    }

    /* If iterations are convereged
     * according to some measuere
     */
    bool isConverged(){
        if (pConvergenceMeasure_){ // NULL corresponds to no measure
            return pConvergenceMeasure_->isConverged();
        }
        return false;
    }


};// end class



//for debugging purposes show firts values of adjacency matrix
void show_adj_matrix(uint const * const * const A_indeces, float const * A_values,
                     const uint nE, uint links_to_show=20){
    links_to_show = (nE < links_to_show) ? nE : links_to_show;
    std::cout << "******** Adjacency Matrix ********\n";
    for(uint i = 0; i < links_to_show; ++i)
        std::cout << A_indeces[i][0] << " " << A_indeces[i][1]
                                     << " " << A_values[i] << std::endl;
    std::cout << std::endl;
    return;
}

}//end anonymous namespace

/* run of one EM algorithm with one random initializaion*/
RET_CODE EM_run(float** & k_iz,
                uint** A_indeces,
                float* A_values,
                AlgorithmParameters const& params,
                float& loglikelihood) {

    uint nK = params.nK_;
    uint nV = params.nV_;
    uint nE = params.nE_;
    uint nIterations = params.nIterations_;

    float D;
    float** k_iz_temp = make2DArray<float>(nK,nV);

    std::vector<float> kz(nK, 0.0f);
    StatisticsGatherer Gatherer(nIterations, params.convergence_measure_,
                                params.convergence_tolerance_);

    if (kDEBUG) {
        //show_adj_matrix(A_indeces, A_values, nE);
        std::cout << "Running EM algorithm.\n";
    }
    uint iter(0);
    while ( !(Gatherer.isConverged() || (iter == nIterations)) ){
        iter++;
        // compute k_z as in formula (10)
        for (uint z=0; z<nK; ++z){
            kz[z] = 0;
            for (uint i=0; i<nV; ++i){
                kz[z] += k_iz[z][i];
                k_iz_temp[z][i] = 0.0;
            }
        }

        // go through all links
//#if defined (_OPENMP)
//#pragma omp parallel for default(none) shared(nE, nK, k_iz, k_iz_temp, A_indeces, kz) private(D) schedule(static)
//#endif
        for (uint link=0; link < nE; ++link){
            int i = A_indeces[link][0];
            int j = A_indeces[link][1];

            //compute D formula (12)
            D = 0.0;
            for (uint z=0; z<nK; ++z){
                D += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
            }

            //compute q_zij formula (13)
            float q_temp;
            for (uint z=0; z<nK; ++z){
                q_temp = k_iz[z][i]*k_iz[z][j]/(D*kz[z]+EPS);
                //update values k_iz_prime according to formula (9)
                k_iz_temp[z][i] += q_temp;
                k_iz_temp[z][j] += q_temp;
            }
        }// finished looping through all links

        //can be openmp'ed
        //update k_iz
        std::swap(k_iz, k_iz_temp);
        /*
#if defined (_OPENMP)
#pragma omp parallel for default(none) shared(nV, nK, k_iz, k_iz_temp) schedule(static, nV/4)
#endif
        for (uint i=0; i<nV;++i){
            for(uint z=0; z<nK; ++z){
                    k_iz[z][i] = k_iz_temp[z][i];
             }
        }
        */
        Gatherer.DumpIteration(k_iz, A_indeces, nE, nV, nK);
    }// finish iteration
    if (kDEBUG) std::cout << "DEBUG: Finish iterations.\n";
    //computing final values of community structures and log_likelihood
    // can be openmp'ed
    // compute k_z as in formula (10)
//#if defined (_OPENMP)
//#pragma omp parallel for default(none) shared(nK, nV, kz, k_iz) schedule(static, 1)
//#endif
    for (uint z=0; z<nK; ++z){
        kz[z] = 0.0;
        for (uint i=0; i<nV; ++i){
            kz[z] += k_iz[z][i];
          }
    }
    loglikelihood=0.0;
    double sum_t; // variable to hold temporary values
    for (uint link = 0; link < nE; ++link)
    {
        sum_t = 0.0;
        uint i = A_indeces[link][0];
        uint j = A_indeces[link][1];
        for (uint z=0;z<nK;z++)
        {
            sum_t += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
        }
        loglikelihood += A_values[link]*log(sum_t + EPS);
    }
    loglikelihood -= nE;
    /*
    for (uint i=0; i < nV; ++i){
        for (uint j=0; j < nV; ++j){
            sum_t = 0.0;
            for (uint z=0;z<nK;z++)
            {
                sum_t += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
            }
            loglikelihood -= sum_t;
        }
    }
   */
    if (kDEBUG) std::cout << "DEBUG: The loglikelihood of this initializaiton:  " << loglikelihood << std::endl;
    Free2DArray<float>(k_iz_temp);
    return (iter == nIterations) ? RET_CODE::MAX_ITERATIONS : RET_CODE::CONVERGED;
}

/* run of one EM algorithm with one random initializaion.
   Includes prunning procedure.*/
RET_CODE EM_run_prunned(float** k_iz, uint** A_indeces,
            //float* A_values, not needed all equal to one
            AlgorithmParameters const& params,
            float& loglikelihood){
    uint nK = params.nK_;
    uint nV = params.nV_;
    uint nE = params.nE_;
    // if the color of edge is converged
    std::vector<bool> is_link_converged(nE, false);
    std::vector<bool> is_vertex_converged(nV, false);
    float threshold = params.zero_threshold_;
    uint nIterations = params.nIterations_;
    float** k_iz_prime = make2DArray<float>(nK,nV);
    std::vector<float> kz(nK, 0.0);

    StatisticsGatherer Gatherer(nIterations, params.convergence_measure_,
                                params.convergence_tolerance_);

    if (kDEBUG) {
        //show_adj_matrix(A_indeces, A_values, nE);
        std::cout << "Running EM algorithm with prunning.\n";
        std::cout << "Threshold: " << threshold << std::endl;
    }
    uint iter(0);
    while ( !(Gatherer.isConverged() || (iter == nIterations)) ){
        iter++;
        // compute k_z as in formula (10)
        for (uint z=0; z<nK; ++z){
            kz[z]=0;
            for (uint i=0; i<nV; ++i){
                kz[z] += k_iz[z][i];
                k_iz_prime[z][i] = 0.0;
            }
        }
        // OPENMP: due to critical section the program is slowed down considerably by 5X on 4 processors
        // go through all links
//#if defined (_OPENMP)
//#pragma omp parallel for default(none) shared(nE, k_iz, is_vertex_converged, is_link_converged, Gatherer, iter, k_iz_prime, threshold, kDEBUG, A_indeces, nK, kz) schedule(static)
//#endif
        for (uint link=0; link < nE; ++link){
            if (is_link_converged[link]){
                if (kDEBUG){
                    Gatherer.DumpPrunning(iter, SKIP_LINK);
                }
                continue;
            }
            int i = A_indeces[link][0];
            int j = A_indeces[link][1];


            //check if link has just converged
            if (is_vertex_converged[i] && is_vertex_converged[j]){
//#pragma omp critical
// {
                is_link_converged[link]=true;
//}
                // the vertes should be already converged
                //assert(is_vertex_converged[i]==true);
                //assert(is_vertex_converged[j]==true);
                if (kDEBUG){
                    Gatherer.DumpPrunning(iter, LINK_CONVERGED);
                }
                continue;
            }

            // the vertex won't change the color anymore
            if (is_vertex_converged[i]){
                for (uint z=0; z<nK; ++z){
                    // converged vertex gives +1.0 contribution across link for one color z
//#pragma omp critical
//{
                    k_iz_prime[z][j] += (k_iz[z][i] > threshold ? 1.0 : 0.0);;
//}
                    }
                if (kDEBUG){
                    Gatherer.DumpPrunning(iter, SKIP_VERTEX);
                }
                continue;
            }
            if (is_vertex_converged[j]){
                for (uint z=0; z<nK; ++z){
                    // converged vertex gives +1.0 contirbution across the link for one color z
 //#pragma omp critical
 //{
                    k_iz_prime[z][i] += (k_iz[z][j] > threshold ? 1.0 : 0.0);
 //}
                }
                if (kDEBUG){
                    Gatherer.DumpPrunning(iter, SKIP_VERTEX);
                }
                continue;
            }

            //compute D formula (12)
            float D = 0.0;
            for (uint z=0; z<nK; ++z){
                D += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
            }

            //compute q_zij formula (13)
            for (uint z=0; z<nK; ++z){
                if (k_iz[z][i] <= threshold || k_iz[z][j] <= threshold){
                    if (kDEBUG){
                        Gatherer.DumpPrunning(iter, SKIP_ZERO_COLOR);
                    }
                    continue;
                }
                float q_temp = k_iz[z][i]*k_iz[z][j]/(D*kz[z]+EPS);
                //update values k_iz_prime according to formula (9)
//#pragma omp critical
//{
                k_iz_prime[z][i] += q_temp;
                k_iz_prime[z][j] += q_temp;
//}
            }
        }// finished looping through all links

        //OPENMP: slighltly slows down the porgram
        //update k_iz
//#if defined (_OPENMP)
//#pragma omp parallel for default(none) shared(nV, nK, is_vertex_converged, k_iz, k_iz_prime, threshold) schedule(dynamic)
//#endif
        for (uint i=0; i<nV;++i){
            if (is_vertex_converged[i])
                continue;
            uint nonzero_colors = 0;
            for(uint z=0; z<nK; ++z){
                if (k_iz_prime[z][i] <= threshold){

                   if (kDEBUG){
                        Gatherer.DumpPrunning(iter, VALUE_BELOW_THRESHOLD);
                        //cout << "Below threshold: " << i << " " << z << std::endl;
                   }
                    k_iz[z][i]=0.0;
                }
                else
                {
                    nonzero_colors += 1;
                    k_iz[z][i] = k_iz_prime[z][i];
                }
            }
            if (nonzero_colors == 1){
                is_vertex_converged[i] = true;
                if (kDEBUG){
                    Gatherer.DumpPrunning(iter, VERTEX_CONVERGED);
                }
            }
        }
        Gatherer.DumpIteration(k_iz, A_indeces, nE, nV, nK);
    }// finish iteration
    if (kDEBUG) std::cout << "DEBUG: Finish iterations.\n";


    // computing final values of community structures and log_likelihood
    // can be openmp'ed
    // compute k_z as in formula (10)
    for (uint z=0; z<nK; ++z){
        kz[z] = 0.0;
        for (uint i=0; i<nV; ++i){
            kz[z] += k_iz[z][i];
          }
    }
    loglikelihood=0.0;
    double sum_t; // variable to hold temporary values
    for (uint link = 0; link < nE; ++link)
    {
        sum_t = 0.0;
        uint i = A_indeces[link][0];
        uint j = A_indeces[link][1];
        for (uint z=0;z<nK;z++)
        {
            sum_t += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
        }
        loglikelihood += log(sum_t + EPS);
    }
    loglikelihood -= nE;
    /*    for (uint i=0; i < nV; ++i){
        for (uint j=0; j < nV; ++j){
            sum_t = 0.0;
            for (uint z=0; z<nK; z++)
            {
                sum_t += k_iz[z][i]*k_iz[z][j]/(kz[z]+EPS);
            }
            loglikelihood -= sum_t;
        }
    }
    */
    if (kDEBUG){
        Gatherer.PrintPrunnings();
    }
    if (kDEBUG) std::cout << "DEBUG: The loglikelihood of this initializaiton:  " << loglikelihood << std::endl;
    Free2DArray<float>(k_iz_prime);

    return (iter == nIterations) ? RET_CODE:: MAX_ITERATIONS : RET_CODE::CONVERGED;
}
