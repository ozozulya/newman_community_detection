#ifndef RANDOMINIT_H
#define RANDOMINIT_H

#include <map>
#include <string>

typedef unsigned int uint;

// I implement it as a functor just in case we want
// to tweak it more seriously later, explore different
// random number generators etc.
class RandomInit {
public:
    virtual void operator()(float**, uint, uint, bool,
                            const std::map<std::string, std::pair<uint, uint> >&,
                            const std::map<std::string, float>&) const = 0;
    virtual ~RandomInit(){} // Don't think we'll need dtors in the derived classes

    virtual void setSeed(long) const = 0;
};


#endif // RANDOMINIT_H
