#ifndef RANDOMINIT__HELPER_H
#define RANDOMINIT__HELPER_H
#include <string>
#include "randominit.h"
#include "randominitfactory.h"
template <class T>
class RandomInitHelper
{
 public:
  RandomInitHelper(std:: string);
  static RandomInit* Create();
};

template <class T>
RandomInit* RandomInitHelper<T>::Create(){
  return new T;
}

template <class T>
RandomInitHelper<T>::RandomInitHelper(std::string id){
  RandomInitFactory& theFactory = RandomInitFactory::Instance();
  theFactory.RegisterRandomInit(id, RandomInitHelper<T>::Create);

}

#endif
