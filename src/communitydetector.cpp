#include "communitydetector.h"
#include <iostream>
#include "EM_core.h"
#include "parameters.h"
#include "randominit.h"
#include <limits>
//#include "omp.h"
namespace{

const bool kDEBUG = false;

/*
 * Normalize the links colors k_iz for each vertex
 * and assign it to k_win, which shows the proportions of
 * each community for a vertex.
 */
void normalizeCommunities(const uint nV, const uint nK,
                          float** k_win){
    std::vector<double> sumi(nV, 0.0);
    for (uint i=0; i<nV; i++)
    {
        sumi[i]=k_win[0][i];
        for (uint z=1; z<nK; z++)
        {
            sumi[i] += k_win[z][i];
        }
    }
    // k_win[z][i]/sumi[i] is the probalility of the vertex "i" to be in
    // community "z"
    for (uint i=0; i<nV ;i++)
        for (uint z=0; z<nK; z++){
            k_win[z][i]= k_win[z][i]/sumi[i];
         }
}
} // end namespace

CommunityDetector::CommunityDetector(const AlgorithmParameters& params,
                  const std::map<std::string, float>& link_weights,
                  const std::map<std::string, std::pair<uint, uint> >& link_edges,
                  const RandomInit& initializer)
    : initializer_(initializer), params_(params),
      link_weights_(link_weights), link_edges_(link_edges)
    {}

void CommunityDetector::run(){
    //links to loop over, some of the original links
    // may be prunned, i.e. we conclude that they will not change
    // the color anymore and leave them alone.
    // TODO: not used at the moment
    // map<std::string, std::pair<uint, uint> > links_to_loop = link_edges_;
    uint nV = params_.nV_;
    uint nK = params_.nK_;
    uint nRandInit = params_.nRandInits_;
    uint ignore_weights = params_.ignore_weights_; //TODO: remove
    float maxloglikelihood=-std::numeric_limits<float>::max();//initial value of maxlikelihood
    likelihoods_.resize(nRandInit);
    float **k_iz,  **k_win;
    uint** A_indeces;
    float* A_values;
    // Allocate all matrices
    A_indeces = make2DArray<uint>(link_edges_.size(), 2);
    A_values = (float* ) malloc(link_edges_.size()*sizeof(float));
    k_win = make2DArray<float>(nK, nV);

    ///////////////// Make adjacency matrix /////////////////////////////////
    std::map<std::string, std::pair<uint, uint> >::const_iterator it = link_edges_.begin();
    for (int i= 0; it != link_edges_.end(); ++it, ++i){
        A_indeces[i][0] = (it->second).first;
        A_indeces[i][1] = (it->second).second;
        if (ignore_weights)
            A_values[i] = 1.0;
        else
            A_values[i] = (link_weights_.find(it->first))->second;
    }

 #if defined (_OPENMP)
#pragma omp parallel for\
    shared(nK, nV, A_indeces, A_values,\
    k_win, maxloglikelihood, nRandInit, ignore_weights)\
    private(k_iz) schedule(dynamic)
 #endif
    for(uint iter=0; iter < nRandInit; ++iter)
    {
/////////////////////// EM algorithm /////////////////////////
        float loglikelihood;
        // each thread should manage its own heap memory
        k_iz = make2DArray<float>(nK,nV);
        // Random initialization
        // initializer is not thread safe
#if defined (_OPENMP)
#pragma omp critical
{
#endif
        initializer_(k_iz, nK, nV, ignore_weights, link_edges_, link_weights_);
#if defined (_OPENMP)
}
#endif
        //till the end of iteration we work with C-like arrays only
        RET_CODE ret;
        if (params_.to_prun_){
            ret = EM_run_prunned(k_iz, A_indeces, params_, loglikelihood);
        }
        else{
            // note that k_iz is passed by reference, because pointer
            // may be changed by the swap operation inside the algorithm.
            ret = EM_run(k_iz, A_indeces,
               A_values, params_, loglikelihood);
        }
        likelihoods_[iter]=loglikelihood;

#if defined (_OPENMP)
#pragma omp critical
{
#endif

        //EM step: keep the largest probability
        if (maxloglikelihood<loglikelihood)
        {
            maxloglikelihood=loglikelihood;
            std::swap(k_iz, k_win);
        }
        std::cout << "Loglikelihood of " << iter << " initialization: "
                  << loglikelihood << std::endl;
        if (ret == RET_CODE::MAX_ITERATIONS){
            std::cout << "Max number of iterations " << params_.nIterations_
                      << " is reached.\n";
        }
        else {
            std::cout << "The iterations have converged.\n";
        }
#if defined (_OPENMP)
}
#endif // openmp: end critical section
        Free2DArray<float>(k_iz);
    }//nRandInit
    
    normalizeCommunities(nV, nK, k_win);

    // reformat the data in std::vector<std::vector>
    arrayToVector(k_win, nK, nV, k_win_);

    // delete all the data
    // float **k_win and A arrays;
   Free2DArray<float>(k_win);
   Free2DArray<uint>(A_indeces);
   free(A_values);
}

