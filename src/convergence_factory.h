#ifndef CONVERGENCE_FACTORY_H
#define CONVERGENCE_FACTORY_H

#include <map>
#include <string>
#include "convergence_measure.h"
#include <functional>

class ConvergenceMeasure;

class ConvergenceFactory
{
    typedef ConvergenceMeasure* (*CreateConvergenceFunction)(double);

public:
    static ConvergenceFactory& Instance();
    void RegisterConvergence(const std::string&, CreateConvergenceFunction);
    ConvergenceMeasure* CreateConvergence(std::string ConvergenceId, double tolerance);
    ~ConvergenceFactory(){}
private:
    std::map<std::string, CreateConvergenceFunction> CreatorFunctions;
    ConvergenceFactory(){}
    // disallow copy and assign
    ConvergenceFactory(const ConvergenceFactory&){}
    ConvergenceFactory& operator=(const ConvergenceFactory&){return *this;}
};

#endif // CONVERGENCE_FACTORY_H
