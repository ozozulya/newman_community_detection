#ifndef UTILS_H
#define UTILS_H
#include <stdlib.h>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>


struct Parameters;

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&);


/*
Templates for different types of matrix elements.
*/

/* Make 2D array of contiguous memory suitable
  for passing with MPI, in case we need it.
  */
typedef unsigned int uint;

template<typename T>
T** make2DArray(const uint nrows, const uint ncols)
{
    T** x = (T**) calloc(nrows, sizeof(T*));
    x[0] = (T*) calloc(nrows*ncols, sizeof(T));
    for (uint i = 1; i < nrows; ++i){
        x[i] = &x[0][i*ncols];
    }
    return x;
}

// TODO: remove it
template<typename T>
std::string ConvertToString(T Number){
    std::ostringstream os;
    os << Number;
    return os.str();
}

template<typename T>
void Free2DArray(T** Matrix){
    free(Matrix[0]);
    free(Matrix);
}

/** Copy the values from a 2d array to a vector */
template<typename T>
bool arrayToVector(T** const arr, const uint& m, const uint& n,
                   std::vector<std::vector<T> >& v){
    v.clear();
    v.resize(m);
    for (uint z=0; z<m ; ++z){
        v[z].resize(n);
        for (uint i=0; i<n; ++i){
            v[z][i] = arr[z][i];
        }
    }
    return true;
}

//show usage
void showHelp();

// Write the ruseults of EM algorithm to the files.
void dumpResults(const Parameters& params,
                  const std::vector<float>& m_likelihood,
                  const std::vector<std::vector<float> >& outputk);

void dumpResults(const Parameters& params,
                  const std::vector<float>& m_likelihood,
                  float** const outputk);

// print results of community detection algorithm
void print2dVector(std::vector<std::vector<float> > const& results);

#endif // UTILS_H
