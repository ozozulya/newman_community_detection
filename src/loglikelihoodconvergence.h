#ifndef LOGLIKELIHOODCONVERGENCE_H
#define LOGLIKELIHOODCONVERGENCE_H
#include "convergence_measure.h"
#include <vector>

class LogLikelihoodConvergence: public ConvergenceMeasure
{
public:
    LogLikelihoodConvergence(double tolerance):
        tolerance_(tolerance), previous_measure_(0.),
        current_measure_(0.)
    {}

    void DumpIteration(float const * const * const k_iz,
                       unsigned int const * const * const A_indeces,
                       unsigned int nE,
                       unsigned int nV,
                       unsigned int nK);
    bool isConverged();

private:
    double tolerance_;
    double previous_measure_, current_measure_;

};

#endif // LOGLIKELIHOODCONVERGENCE_H
