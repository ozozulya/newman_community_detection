#ifndef SCALARDIFF_CONVERGENCE_H
#define SCALARDIFF_CONVERGENCE_H
#include <vector>
#include "convergence_measure.h"

class ScalarDiffConvergence: public ConvergenceMeasure
{
public:
    ScalarDiffConvergence(double tolerance):
        tolerance_(tolerance), current_measure_(1.)
    {}

    void DumpIteration(float const * const * const k_iz,
                       unsigned int const * const * const A_indeces,
                       unsigned int nE,
                       unsigned int nV,
                       unsigned int nK);
    bool isConverged();

private:
    double tolerance_, current_measure_;
    std::vector<std::vector<float> > k_iz_previous_;
};
#endif // SCALARDIFF_CONVERGENCE_H
