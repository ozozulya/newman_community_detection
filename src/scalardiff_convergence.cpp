#include "scalardiff_convergence.h"
#include <cmath>

#include <iostream>
namespace{
  float EPS = 1.0E-18;
  bool kDEBUG = false;
}

typedef unsigned int uint;

bool ScalarDiffConvergence::isConverged()
{
    //first iteration impossible ever to converge
    // one subtract k_iz from 0 everywhere.
    if (current_measure_ < tolerance_){
        return true;
    }
    else {
        return false;
    }
}

void ScalarDiffConvergence::DumpIteration(float const * const * const k_iz,
                                             uint const * const * const A_indeces,
                                             uint nE,
                                             uint nV, uint nK)
{
    if (k_iz_previous_.empty()){
        k_iz_previous_.resize(nK);
        for (uint z=0; z<nK; ++z){
            k_iz_previous_[z].resize(nV, 0.f);
        }
    }

    double kz_diff, max_diff(0.);
    for (uint z=0; z<nK; ++z){
        kz_diff = 0.0;
        for (uint i=0; i<nV; ++i){
            kz_diff += fabs(k_iz[z][i] - k_iz_previous_[z][i]);
            k_iz_previous_[z][i] = k_iz[z][i];
          }
        if (kz_diff > max_diff)
            max_diff = kz_diff;
    }
    current_measure_ = max_diff/nV;
    if (kDEBUG){
        std::cout << current_measure_ << std::endl;
    }
}
