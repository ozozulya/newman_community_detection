/* A factory for random initialzation functors.
 *
 */

#ifndef RANDOMINITFACTORY_H
#define RANDOMINITFACTORY_H

#include <map>
#include <string>

// forward declaration
class RandomInit;

class RandomInitFactory
{
   typedef RandomInit* (*CreateRandomInitFunction)(void);
public:

    static RandomInitFactory& Instance();
    void RegisterRandomInit(std::string, CreateRandomInitFunction);
    RandomInit* CreateRandomInitializer(std::string RandomInitializerId);
    ~RandomInitFactory(){}
private:
    std::map<std::string, CreateRandomInitFunction> CreatorFunctions;
    RandomInitFactory(){}
    // disallow copy and assign
    RandomInitFactory(const RandomInitFactory&){}
    RandomInitFactory& operator=(const RandomInitFactory&){ return *this;}
};

#endif // RANDOMINITFACTORY_H
