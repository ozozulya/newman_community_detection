#include <iostream>
#include <boost/program_options.hpp>

#include "parse_args.h"
#include "parameters.h"
#include "community_config.h"

namespace po = boost::program_options;

namespace{
    const double kTOLERANCE = 1.E-6;
}
void showHelp(){
    std::cout << "Specify the following command line options:\n"
         << "-f : Input file name\n"
         << "-k : Number of communities\n"
         << "-v : number of nodes\n"
         << "-s, --steps : number of EM steps (optional, default 100)\n"
         << "-n, --rand_inits : number of random initializations (optional, default 5)\n"
         << "-o : output file (optional)\n"
         << "-h, --help : show this help\n"
         << "-w, --weights : set 1 to ignore weights, 0 to include, DISABLED\n"
         << "-r, --rand_initializer : choose random initialization scheme, \"one\" or \"two\" \n"
         << "-t, --threshold : float, prun vertices to zero whose value falles below threshold.\n"
         << "                  This reduces precision of the result.\n"
         << "-m, --convergence_measure : convergence measure string for stopping itertaions.\n"
         << "                           Available options: loglikelihood_convergence,\n"
         << "                                               scalardiff_convergence.\n"
         << "--measure_tolerance : error tolerance if convergence measure is enabled.\n"
         << "                      Default: " << kTOLERANCE << std::endl
         << "Version: " << community_detection_VERSION_MAJOR << "."
         << community_detection_VERSION_MINOR << std::endl;
}

bool parseArguments(int argc, char **argv, Parameters &params){
    std::string data_fname("");
    std::string result_fname(""), progress_fname;
    uint nV;// number of nodes in the network
    uint nK;// number of communities;
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 5;// number of random initializations
    std::string convergence_measure("UNKNOWN"); // enable converrgance if it is set.
    double convergence_tolerance = kTOLERANCE;
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    float threshold = 0.0;
    bool to_prun = false;

   // read paramters from command line


//    args >> GetOpt::Option('r', "rand_initializer", rand_initializer);
//    args >> GetOpt::Option('m', "convergence_measure", convergence_measure);
//    args >> GetOpt::Option("measure_tolerance", convergence_tolerance);
//    if (args >> GetOpt::Option('t', "threshold", threshold)){
//        to_prun=true;
//    };

// Declare the supported options.

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce help message")
            ("input-file,f", po::value<std::string>(&data_fname), "input file")
            ("communities,k", po::value<uint>(&nK), "number of communities")
            ("vertices,v", po::value<uint>(&nV), "number of vertices")
            ("steps,s", po::value<uint>(&nIterations)->default_value(100), "number of iteration steps")
            ("rand-inits,n", po::value<uint>(&nRandInit)->default_value(5), "number of random initializations")
            ("output-file,o", po::value<std::string>(&result_fname)->default_value(""), "output file")
            ("ignore-weights,i", po::value<bool>(&ignore_weights)->default_value(false), "ignore weights of the graph")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
    }

    // create a new output filename if it was not specified
    size_t last_dot=data_fname.rfind('.');
    if (last_dot == std::string::npos)
        last_dot = data_fname.length()-1;
    char temp[4096];
    if (result_fname==""){
        sprintf(temp, "%s_result_steps_%d_inits_%d.txt",
                data_fname.substr(0, last_dot).c_str(), nIterations, nRandInit);
        result_fname = temp;
         }
    sprintf(temp, "%s_progress.txt",
                data_fname.substr(0, last_dot).c_str());
    progress_fname = temp;


    AlgorithmParameters alg = {
        nK, nV, 0,
        nIterations,
        nRandInit,
        ignore_weights,
        to_prun,
        threshold,
        convergence_measure,
        convergence_tolerance
    };
    DataFileParameters data = {
        data_fname,
        result_fname,
        progress_fname
    };
    params = Parameters(alg, data);
    return true; //successful parse of the parameters
}
