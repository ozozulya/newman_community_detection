#ifndef PARAMETERS_H
#define PARAMETERS_H
#include <string>
typedef unsigned int uint;

struct AlgorithmParameters {
    uint nK_; // number of communities
    uint nV_; // number of vertices, nodes
    uint nE_; // number of edges (aka links)
    uint nIterations_; // number of iterations
    uint nRandInits_;
    bool ignore_weights_; // ingore weight of the links, always true
    bool to_prun_; // ? to run algorithm with prunning if yes
    float zero_threshold_; // below it all is 0.
    std::string convergence_measure_;
    double convergence_tolerance_;
};

struct DataFileParameters {
    std::string data_fname_; // name of the file with the graph
    std::string result_fname_; // the best result
    std::string progress_fname_; // progress output
};

struct Parameters: public AlgorithmParameters, DataFileParameters {
    Parameters(AlgorithmParameters, DataFileParameters);
    Parameters(){}
};


#endif // PARAMETERS_H
