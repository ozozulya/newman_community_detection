#include "utils.h"
#include <iostream>
#include "parameters.h"
#include <string>
#include <sstream>
#include <fstream>
#include "community_config.h"


void dumpResults(const Parameters& params,
                  const std::vector<float>& m_likelihood,
                  const std::vector<std::vector<float> >& k_win){

    std::ofstream oresult;
    oresult.open(params.result_fname_.c_str());
    // TODO: check if the files have been open
    for (uint i=0; i<params.nV_; i++)
    {
        oresult << i;
        for (uint z=0; z<params.nK_; z++) {
            oresult << ' ' << k_win[z][i];
        }
        oresult << std::endl;
    }

    /*std::vector<float>::const_iterator it = m_likelihood.begin();
    for (uint i=0; it != m_likelihood.end(); ++i, ++it)
    {
        oprogress << "For iteration "<<i+1<<" the loglikelihood is "
                  << m_likelihood[i] << std::endl;
    }
    //output ends
    oprogress << "\nThe maximum loglikelihood  is "
              << *std::max_element(m_likelihood.begin(), m_likelihood.end())
              << std::endl;
    */
    oresult.close();
    //oprogress.close();
}

void dumpResults(const Parameters& params,
                  const std::vector<float>& m_likelihood,
                  float** const outputk){
     std::vector<std::vector<float> > v;
     arrayToVector<float>(outputk,  params.nK_, params.nV_, v);
     dumpResults(params, m_likelihood, v);
}


void print2dVector(std::vector<std::vector<float> > const& results){
    for(uint i = 0; i < results[0].size(); ++i){
        std::cout << i << " : ";
        for (uint j = 0; j < results.size(); ++j)
            std::cout << results[j][i] << " ";
        std::cout << std::endl;
    }
}
