#ifndef EM_CORE_H
#define EM_CORE_H
#include <cstdint>
struct AlgorithmParameters;

enum class RET_CODE : uint8_t
{
    CONVERGED,
    MAX_ITERATIONS
};

typedef unsigned int uint;

/* run of one EM algorithm with one random initializaion
   reference is used in the first argument, because I
   swap the pointers in the program to avoid copying arrays.
*/
RET_CODE EM_run(float** & k_iz, uint** A_indeces,
            float * A_values,
            AlgorithmParameters const& params,
            float& loglikelihood);


RET_CODE EM_run_prunned(float** k_iz, uint** A_indeces,
            //float* A_values, not needed all equal to one
            AlgorithmParameters const& params,
            float& loglikelihood);

#endif // EM_CORE_H
