#include "park_miller.h"

const long a = 16807; // 7^5
const long m = 2147483647; // = 2^31-1 Mersenne prime
const long q = 127773; //
const long r = 2836;


ParkMiller::ParkMiller(long seed) {
    seed_ = seed;
    if (seed_ == 0)
        seed_ = 1;
}

void ParkMiller::SetSeed(long seed){
    seed_=seed;
    if (seed_ == 0)
        seed_ = 1;
}

unsigned long ParkMiller::Max(){
    return m-1;
}

unsigned long ParkMiller::Min(){
    return 1;
}

long ParkMiller::GetOneRandomInteger(){

    long k = seed_/q;
    seed_ = a*(seed_-k*q) - r*k;
    if (seed_ < 0)
        seed_ += m;
    return seed_;
}
