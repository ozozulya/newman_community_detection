// Header file for VERTEX, EDGE, and NETWORK data structures
//
// Mark Newman  11 AUG 06
// Rewritten for C++  by O. Zozulya AUG 2012
#include<set>
#include<string>
#include<vector>
#ifndef _NETWORK_H
#define _NETWORK_H

typedef unsigned int uint;
// Here T is either float or double
template<typename T=double>
 struct EDGE {
  size_t target;        // Index in the vertex[] array of neighboring vertex.
                     // (Note that this is not necessarily equal to the GML
                     // ID of the neighbor if IDs are nonconsecutive or do
                     // not start at zero.)
  T weight;     // Weight of edge.  1 if no weight is specified.
  EDGE(): target(-1), weight(0.0) {}
  EDGE(size_t target_, T weight_): target(target_), weight(weight_){}
};

struct VERTEX{
  int id;            // GML ID number of vertex
  uint degree;        // Degree of vertex (out-degree for directed nets)
  std::string label;       // GML label of vertex.  NULL if no label specified
  std::vector<EDGE<double> > edges;        // Array of EDGE structs, one for each neighbor
  VERTEX(): id(-1), degree(0){}
};


// This is to sort the vertices in std::vector<VERTEX>
//so one can perform binary search later.
struct VertexLessThen{
    inline bool operator()(const VERTEX& a, const VERTEX& b) const{
        return (a.id < b.id);
    }
    inline bool operator()(const VERTEX& a, const int& id) const{
        return (a.id < id);
    }
};


struct NETWORK{
  size_t nvertices;     // Number of vertices in network
  bool directed;      // true = directed network, false = undirected
  std::vector<VERTEX> vertices;    // set of VERTEX structures
};

#endif
