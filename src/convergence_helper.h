#ifndef CONVERGENCE_HELPER_H
#define CONVERGENCE_HELPER_H

#include <string>
#include "convergence_measure.h"
#include "convergence_factory.h"
template <class T>
class ConvergenceHelper
{
 public:
  ConvergenceHelper(std:: string);
  static ConvergenceMeasure* Create(double);
};

template <class T>
ConvergenceMeasure* ConvergenceHelper<T>::Create(double tolerance){
    return new T(tolerance);
}

template <class T>
ConvergenceHelper<T>::ConvergenceHelper(std::string id){
  ConvergenceFactory& theFactory = ConvergenceFactory::Instance();
  theFactory.RegisterConvergence(id, ConvergenceHelper<T>::Create);
}

#endif // CONVERGENCE_HELPER_H
