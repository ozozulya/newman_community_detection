#include <iostream>
#include <iterator>
#include <math.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <map>
#include <utility>
#include <numeric>
#include <algorithm>
#include "load_data.h"
#include "randominit.h"
#include "parameters.h"
#include "EM_core.h"
#include "utils.h"
#include "randominitfactory.h"
#include "parse_args.h"
#include "communitydetector.h"
#include <memory> // smart pointers

typedef unsigned int uint;



namespace {
const bool kDEBUG = true;
}//end anonymous namespace


int main(int argc, char* argv[])
{
    if (argc==1){
        showHelp();
        return 0;
    }
    clock_t start,finish;
    double totaltime;
    float **outputk;
    Parameters params;
    if (!parseArguments(argc, argv, params)){
        showHelp();
        return EXIT_SUCCESS;
    }

    outputk = make2DArray<float>(params.nK_, params.nV_);
    std::map<std::string, float> link_weights;
    std::map<std::string, std::pair<uint, uint> > link_edges;

    // load the data from the file
    load_data(params.data_fname_, link_weights, link_edges);

    // number of edges can be initialized only after reading the graph.
    params.nE_ = link_edges.size();
    std::unique_ptr<RandomInit> initializer_ptr {RandomInitFactory::Instance().
            CreateRandomInitializer("RandomInitializer")}; // hardcoded because I remove another initializer

    // set a random seed
    srand(time(NULL));
    initializer_ptr->setSeed(rand());
    start=clock();

    ///////////////////////////////////////////////////////
    // Run the algorithm
    ///////////////////////////////////////////////////////

	CommunityDetector Detector{ params, link_weights,
							   link_edges,
							   *initializer_ptr };
    Detector.run();
    finish=clock();
	// TODO: replace with chrono
    totaltime=(double)(finish-start)/CLOCKS_PER_SEC;
    std::cout << "\nRuntime: " << totaltime <<" s" << std::endl;
    dumpResults(params, Detector.get_likelihoods(),
                 Detector.get_result());
    Free2DArray<float>(outputk);
    return 0;
}//main
