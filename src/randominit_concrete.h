#ifndef RANDOMINTI_CONCRETE_H
#define RANDOMINTI_CONCRETE_H
#include "randominit.h"
#include "park_miller.h"
#include <utility>
#include <iterator>
#include <algorithm>
#include <vector>
#include <numeric>
#include <iostream>


namespace {
const bool  kDEBUG =  false;
const long kSEED = 1;  // seed for park-miller random generator
}// end namespace


template<class T = ParkMiller>
class RandomInitTwo: public RandomInit {

    mutable T generator_;

public:
    RandomInitTwo(long seed=1){
        T generator_(seed);
    }


    /* Each link is initially a  assigned random mixture of colors.
      */

    void operator()(float **k_iz, uint nK, uint nV, bool ignore_weights,
                                   const std::map<std::string, std::pair<uint, uint> >& links_to_loop,
                                   const std::map<std::string, float>& link_weights) const {
        for (uint z=0;z<nK;z++)
        {
            for (uint i=0;i<nV;i++)
            {
                k_iz[z][i]=0.0;
            }
        }
        if (kDEBUG)
            std::cout << "RandomInitTwo: ignore_weights: " << ignore_weights << std::endl;
        // initialize k_iz
        std::map<std::string, std::pair<uint, uint> >::const_iterator it = links_to_loop.begin();
        for (;it != links_to_loop.end(); ++it){
            uint i = (it->second).first;
            uint j = (it->second).second;
            std::vector<float> probs(nK);
            for (uint z=0; z<nK; ++z){
                probs[z] = generator_.GetOneRandomInteger()/static_cast<float>(T::Max());
            }
            float norm = std::accumulate(probs.begin(), probs.end(), 0.0);
            if (ignore_weights){
                for (uint z=0; z<nK; ++z){
                    k_iz[z][i] += probs[z]/norm;
                    k_iz[z][j] += probs[z]/norm;
                }
            }
            else{
                float w = (link_weights.find(it->first))->second;
                for (uint z=0; z<nK; ++z){
                    k_iz[z][i] += w*probs[z]/norm;
                    k_iz[z][j] += w*probs[z]/norm;
                }
            }
        }

    }

    void setSeed(long seed) const {
        generator_.SetSeed(seed);
    }
    //~RandomInitOne(){};
};

//template<class T = ParkMiller>
//class RandomInitOne: public RandomInit {
//
//    mutable T generator_;
//
//public:
//    RandomInitOne(long seed=1){
//        T generator_(seed);
//    }
//
//    /*
//      The initialization is performed by assigning a random
//      color to a node corrsponding to the existing link.
//      The potential problem is that if a node has only one link
//      then it will get only one color without mixture of other colors
//      and therefore this initial random color will not be changed.
//      */
//
//    void operator()(float **k_iz, const uint nK, const uint nV, const bool ignore_weights,
//                                   const std::map<std::string, std::pair<uint, uint> >& links_to_loop,
//                                   const std::map<std::string, float>& link_weights) const {
//        for (uint z=0;z<nK;z++)
//        {
//            for (uint i=0;i<nV;i++)
//            {
//                k_iz[z][i]=0.0;
//            }
//        }
//        // initialize k_iz
//        if (kDEBUG)
//            std::cout << "RandomInitOne: ignore_weights: " << ignore_weights << std::endl;
//        std::map<std::string, std::pair<uint, uint> >::const_iterator it = links_to_loop.begin();
//        for (;it != links_to_loop.end(); ++it){
//            int i = (it->second).first;
//            int j = (it->second).second;
//            int z = static_cast<int>(generator_.GetOneRandomInteger() % nK);
//            if (ignore_weights){
//                k_iz[z][i] += 1;
//                k_iz[z][j] += 1;
//            }
//            else{
//                float w = (link_weights.find(it->first))->second;
//                k_iz[z][i] += w;
//                k_iz[z][j] += w;
//            }
//
//        }
//    }
//    void SetSeed(long seed) const{
//        generator_.SetSeed(seed);
//    }
//    //~RandomInitTwo(){};
//};







#endif // RANDOMINTI_CONCRETE_H
