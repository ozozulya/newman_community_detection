#ifndef TESTGRAPH_H
#define TESTGRAPH_H
#include <map>
#include <utility>
#include <vector>
#include <assert.h>
#include <list>
#include <set>
#include <iterator>
#include <iostream>
#include <algorithm>
#include "utils.h"


/* The Bridge patthern is used to separate abstraction/interface from
 * implementation in TestGraph. In this case TestGraphBridge takes care
 * of it's own memory management, while allowing polymorphic behavior like
 * an abstract class.
 */


typedef unsigned int uint;
typedef std::map<std::string, float> link_weights_t;
typedef std::map<std::string, std::pair<uint, uint> > link_vertices_t;

class TestGraphInner{
public:
    virtual link_weights_t get_link_weights() const = 0;
    virtual link_vertices_t get_link_edges() const = 0;
    virtual double TestResult(const std::vector<std::vector<float> >& result) const = 0;
    virtual TestGraphInner* clone() const = 0;
    virtual ~TestGraphInner(){}
};

class TestGraphBridge{ //TODO: C++11 final
public:
    TestGraphBridge(const TestGraphInner& original);
    TestGraphBridge(const TestGraphBridge& original);
    TestGraphBridge& operator=(const TestGraphBridge& original);
    link_weights_t get_link_weights() const;
    link_vertices_t get_link_edges() const;
    double TestResult(const std::vector<std::vector<float> >& result) const;
    ~TestGraphBridge();
private:
    TestGraphInner* TestGraphObj_;
};

inline link_vertices_t TestGraphBridge::get_link_edges() const
{return TestGraphObj_->get_link_edges();}

inline link_weights_t TestGraphBridge::get_link_weights() const
{return TestGraphObj_->get_link_weights();}

inline double TestGraphBridge::TestResult(const std::vector<std::vector<float> >& result) const
{return TestGraphObj_->TestResult(result);}

class TestGraphSynthetic: public TestGraphInner
{
public:
    TestGraphSynthetic(); //TODO: it is better to remove default consturctor
    virtual bool GenerateGraph(uint nV, //total number of vertices
                                uint nA, //number of vertices in the A community
                                uint nB, //number of vertices in the B community
                                uint k); // degree of the Vertex)
    virtual link_weights_t get_link_weights() const;
    virtual link_vertices_t get_link_edges() const;
    virtual double TestResult(const std::vector<std::vector<float> >& result) const;
    // for debugging purpose, non const because we create adj list
    void PrintGraph(uint=50) ;
    virtual TestGraphInner* clone() const;
    virtual ~TestGraphSynthetic(){}
private:
    static const double tolerance_; //FIXME: remove it from here
    uint nV_; // number of vertices
    uint nE_; // number of links(edges)
    uint nK_; // = 2 # number of communities
    uint nA_; // # vertices in the community A; indexed [0..nA_-1]
    uint nB_; // # vertices in the community B; indexed [nA_..nB_+nA_-1]
    link_weights_t link_weights_;
    link_vertices_t link_vertices_;
    // In BoostGraph aka adjacency_list_ representation of the graph
    std::vector<std::list<uint> > adjacency_list_; // vector with the list of connected vertices
    // proportions in which a given vertix belongs to different communites
    // this should be recovered by community structure algorithm
    std::map<uint, std::vector<float> > vertix_communities_;
    bool CheckParamsConsistency(uint, uint, uint, uint);
    void CreateAdjacencyList();
};



#endif // TESTGRAPH_H
