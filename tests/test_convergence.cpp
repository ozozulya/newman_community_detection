#include <gtest/gtest.h>
#include "testgraph.h"
#include <iostream>
#include <string>
#include "community_config.h"
#include "parameters.h"
#include "utils.h"
#include "communitydetector.h"
#include "randominit.h"
#include "randominitfactory.h"

namespace{
// The fixture for testing class Foo.
 class GraphSynthTestConvergence : public ::testing::Test {
 protected:

   virtual void SetUp() {
     // Code here will be called immediately after the constructor (right
     // before each test).
   }

   virtual void TearDown() {
     // Code here will be called immediately after each test (right
     // before the destructor).
   }

   // Objects declared here can be used by all tests in the test case for Foo.
 };


TEST_F(GraphSynthTestConvergence, SmallCommunityDetectionNaiveLogConvergenceZero){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(50), nA(20), nB(20), nK(2);
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 6));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string convergence_measure("loglikelihood_convergence");
    double convergence_tolerance = 0.;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
                       };

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer("RandomInitializer");
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}

TEST_F(GraphSynthTestConvergence, SmallCommunityDetectionNaiveScalarConvergenceZero){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(50), nA(20), nB(20), nK(2);
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 6));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two"); // name of concrete initializer
    std::string convergence_measure("scalardiff_convergence");
    double convergence_tolerance = 0.;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
                       };

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


TEST_F(GraphSynthTestConvergence, SmallCommunityDetectionNaiveConvergence){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(50), nA(20), nB(20), nK(2);
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 6));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two"); // name of concrete initializer
    std::string convergence_measure("loglikelihood_convergence");
    double convergence_tolerance = 1.E-6;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


TEST_F(GraphSynthTestConvergence, SmallCommunityDetectionNaiveScalarConvergencePrunned){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(50), nA(20), nB(20), nK(2);
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 6));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.1;
    bool to_prun = true;
    std::string rand_initializer("two"); // name of concrete initializer
    std::string convergence_measure("scalardiff_convergence");
    double convergence_tolerance = 1.E-2;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


TEST_F(GraphSynthTestConvergence, LargeCommunityDetectionNaive){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 50; // number of EM steps
    uint nRandInit = 1; // number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two");
    std::string convergence_measure("UNKNOWN");
    double convergence_tolerance = 0.;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
                       };
    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


TEST_F(GraphSynthTestConvergence, LargeCommunityDetectionNaiveConvergenceZero){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 50;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two");
    std::string convergence_measure("loglikelihood_convergence");
    double convergence_tolerance = 0.;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       rand_initializer,
                       convergence_tolerance
                       };
    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}

TEST_F(GraphSynthTestConvergence, LargeCommunityDetectionNaiveConvergence){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 50;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two");
    std::string convergence_measure("loglikelihood_convergence");
    double convergence_tolerance = 1.E-6;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
                       };
    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}



TEST_F(GraphSynthTestConvergence, LargeCommunityDetectionNaiveScalarConvergence){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 50;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two");
    std::string convergence_measure("scalardiff_convergence");
    double convergence_tolerance = 1.E-6;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       convergence_measure,
                       convergence_tolerance
                       };
    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    // print2dVector(Detector.get_result()cd -);
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


}//end namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
