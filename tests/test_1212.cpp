// test the output of the example
// with 1212 papers

#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "community_config.h"
#include "parameters.h"
#include "utils.h"
#include "load_data.h"
#include "communitydetector.h"
#include "randominit.h"
#include "randominitfactory.h"
#include <map>
#include <boost/filesystem.hpp>

namespace{
// The fixture for testing class Foo.
 class TestPapers1212 : public ::testing::Test {
 protected:

   virtual void SetUp() {
    // load date and initialize paramters
   }

   virtual void TearDown() {
     // Code here will be called immediately after each test (right
     // before the destructor).
   }

   // Objects declared here can be used by all tests in the test case for Foo.
 };


 /* Test is if the results are correct, 709 paper in the first category, the rest is in the
    second.*/
 bool test_1212_result(std::vector<std::vector<float> > result){
 /*
  Test a particular case of the 1212 papers check that the first 710 papers belong
  to one category and the next 502 to another.
  */
     bool test_passed = false;
     // sanity checks
     if (result.size() != 2) return false;
     if (result[0].size() != 5058) return false;
     int first_cat(0), second_cat(0);
     for (uint i = 0; i < 709; ++i){
         first_cat += result[0][i] > result[1][i] ? +1 : -1;
     }
     for (uint i = 709; i < 1212; ++i){
         second_cat += result[0][i] > result[1][i] ? +1 : -1;
     }
     // some magic numbers, which assume that algorithm was close enough
     // 10 mistakes for the first and second group: (709-10*2) \approx 690,
     // (502-10*2) \approx 480
     if ((second_cat*first_cat < 0) && (std::abs(first_cat) > 690)
         && abs(second_cat) > 480){
        std::cout << "\e[32mThe test was passed\e[0m\n";
        test_passed = true;
    }
    else
        std::cout << "\e[31mThe test was failed\nMay be it's ok and you"
                  << " were just unlucky.\e[0m\n";
    //TODO: more meaningfull message in case of failure
    std::cout << "First category: " << first_cat
              << "\nSecond category: " << second_cat << std::endl;

    return test_passed;
 }



 TEST_F(TestPapers1212, CheckResult){
   // initialize paramters
   std::string test_dir = community_detection_TEST_DIR;
   std::string data_fname = test_dir + "/test_1212_data.txt";
   std::string result_fname = test_dir + "/test_1212_result.txt";
   std::string progress_fname = test_dir + "/test_1212_progress.txt";
   // check if file exists
   boost::filesystem::path p(data_fname);
   boost::filesystem::file_status s = boost::filesystem::status(p);
   EXPECT_EQ(true, boost::filesystem::is_regular_file(s));
   //initialize the file names with data
    DataFileParameters file_params = {data_fname,
                                     result_fname,
                                     progress_fname};


   uint nV=5058; // number of nodes
   uint nK=2; //number of communities
   uint nIterations = 100;// number of EM steps
   uint nRandInit = 5;// number of random initializations
   bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
   bool to_prun = false; // don't prun
   float threshold = 0.0f;
   std::map<std::string, std::pair<uint, uint> > link_edges;
   std::map<std::string, float> link_weights;
   AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                                      ignore_weights, to_prun, threshold};
   // load data
   load_data(data_fname, link_weights, link_edges);
   algo_params.nE_ = link_edges.size(); // TODO: this should be set somwhere else
   RandomInit* initializer_ptr =
           RandomInitFactory::Instance().CreateRandomInitializer("RandomInitializer");

   // TODO: check if RandomInit == NULL;
   CommunityDetector Detector(algo_params, link_weights, link_edges,
                       *initializer_ptr);
   // run the algorithm
   Detector.run();
   Parameters params(algo_params, file_params);
   dumpResults(params, Detector.get_likelihoods(), Detector.get_result());
   // check the output
   EXPECT_EQ(true, test_1212_result(Detector.get_result()));
   delete initializer_ptr;
}

 TEST_F(TestPapers1212, CheckResultPrunned){
   // initialize paramters
   std::string test_dir = community_detection_TEST_DIR;
   std::string data_fname = test_dir + "/test_1212_data.txt";
   std::string result_fname = test_dir + "/test_1212_result.txt";
   std::string progress_fname = test_dir + "/test_1212_progress.txt";
   // check if file exists
   boost::filesystem::path p(data_fname);
   boost::filesystem::file_status s = boost::filesystem::status(p);
   EXPECT_EQ(true, boost::filesystem::is_regular_file(s));
   //initialize the file names with data
   DataFileParameters file_params = {data_fname,
                                     result_fname,
                                     progress_fname};


   uint nV=5058; // number of nodes
   uint nK=2; //number of communities
   uint nIterations = 100;// number of EM steps
   uint nRandInit = 5;// number of random initializations
   bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
   float threshold = 0.01f;
   bool to_prun = true;
   std::map<std::string, std::pair<uint, uint> > link_edges;
   std::map<std::string, float> link_weights;
   AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                                      ignore_weights, to_prun, threshold};
   // load data
   load_data(data_fname, link_weights, link_edges);
   algo_params.nE_ = link_edges.size(); // TODO: this should be set somwhere else
   RandomInit* initializer_ptr =
           RandomInitFactory::Instance().CreateRandomInitializer("RandomInitializer");

   // TODO: check if RandomInit == NULL;
   CommunityDetector C(algo_params, link_weights, link_edges,
                       *initializer_ptr);
   // run the algorithm
   C.run();
   Parameters params(algo_params, file_params);
   dumpResults(params, C.get_likelihoods(), C.get_result());
   // check the output
   EXPECT_EQ(true, test_1212_result(C.get_result()));
   delete initializer_ptr;
}


 TEST_F(TestPapers1212, CheckResultConvergenceDiff){
   // initialize paramters
   std::string test_dir = community_detection_TEST_DIR;
   std::string data_fname = test_dir + "/test_1212_data.txt";
   std::string result_fname = test_dir + "/test_1212_result.txt";
   std::string progress_fname = test_dir + "/test_1212_progress.txt";
   // check if file exists
   boost::filesystem::path p(data_fname);
   boost::filesystem::file_status s = boost::filesystem::status(p);
   EXPECT_EQ(true, boost::filesystem::is_regular_file(s));
   //initialize the file names with data
   DataFileParameters file_params = {data_fname,
                                     result_fname,
                                     progress_fname};


   uint nV=5058; // number of nodes
   uint nK=2; //number of communities
   uint nIterations = 100; // number of EM steps
   uint nRandInit = 5; // number of random initializations
   bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
   float threshold = 0.f;
   bool to_prun = false;
   std::map<std::string, std::pair<uint, uint> > link_edges;
   std::map<std::string, float> link_weights;
   std::string convergence_measure("scalardiff_convergence");
   double convergence_tolerance = 1.E-2;
   AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                      ignore_weights,
                      to_prun,
                      threshold,
                      convergence_measure,
                      convergence_tolerance
                      };
   load_data(data_fname, link_weights, link_edges);
   algo_params.nE_ = link_edges.size(); // TODO: this should be set somwhere else
   RandomInit* initializer_ptr =
           RandomInitFactory::Instance().CreateRandomInitializer("RandomInitializer");

   // TODO: check if RandomInit == NULL;
   CommunityDetector C(algo_params, link_weights, link_edges,
                       *initializer_ptr);
   // run the algorithm
   C.run();
   Parameters params(algo_params, file_params);
   dumpResults(params, C.get_likelihoods(), C.get_result());
   // check the output
   EXPECT_EQ(true, test_1212_result(C.get_result()));
   delete initializer_ptr;
}

 TEST_F(TestPapers1212, CheckResultConvergenceDiffPrunned){
   // initialize paramters
   std::string test_dir = community_detection_TEST_DIR;
   std::string data_fname = test_dir + "/test_1212_data.txt";
   std::string result_fname = test_dir + "/test_1212_result.txt";
   std::string progress_fname = test_dir + "/test_1212_progress.txt";
   // check if file exists
   boost::filesystem::path p(data_fname);
   boost::filesystem::file_status s = boost::filesystem::status(p);
   EXPECT_EQ(true, boost::filesystem::is_regular_file(s));
   //initialize the file names with data
   DataFileParameters file_params = {data_fname,
                                     result_fname,
                                     progress_fname};


   uint nV=5058; // number of nodes
   uint nK=2; //number of communities
   uint nIterations = 100; // number of EM steps
   uint nRandInit = 5; // number of random initializations
   bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
   float threshold = 0.01f;
   bool to_prun = true;
   std::map<std::string, std::pair<uint, uint> > link_edges;
   std::map<std::string, float> link_weights;
   std::string convergence_measure("scalardiff_convergence");
   double convergence_tolerance = 1.E-2;
   AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                      ignore_weights,
                      to_prun,
                      threshold,
                      convergence_measure,
                      convergence_tolerance
                      };
   load_data(data_fname, link_weights, link_edges);
   algo_params.nE_ = link_edges.size(); // TODO: this should be set somwhere else
   RandomInit* initializer_ptr =
           RandomInitFactory::Instance().CreateRandomInitializer("RandomInitializer");

   // TODO: check if RandomInit == NULL;
   CommunityDetector C(algo_params, link_weights, link_edges,
                       *initializer_ptr);
   // run the algorithm
   C.run();
   Parameters params(algo_params, file_params);
   dumpResults(params, C.get_likelihoods(), C.get_result());
   // check the output
   EXPECT_EQ(true, test_1212_result(C.get_result()));
   delete initializer_ptr;
}


}//end namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
