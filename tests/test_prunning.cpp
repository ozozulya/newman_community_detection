#include <gtest/gtest.h>
#include "testgraph.h"
#include <iostream>
#include <string>
#include "community_config.h"
#include "parameters.h"
#include "utils.h"
#include "communitydetector.h"
#include "randominit.h"
#include "randominitfactory.h"

namespace{
// The fixture for testing class Foo.
 class GraphSynthTest : public ::testing::Test {
 protected:

   virtual void SetUp() {
     // Code here will be called immediately after the constructor (right
     // before each test).
   }

   virtual void TearDown() {
     // Code here will be called immediately after each test (right
     // before the destructor).
   }

   // Objects declared here can be used by all tests in the test case for Foo.
 };


TEST_F(GraphSynthTest, SmallCommunityDetectionNaive){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(50), nA(20), nB(20), nK(2);
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 6));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two"); // name of concrete initializer
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


TEST_F(GraphSynthTest, SmallCommunityDetectionPrunned){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(50), nA(20), nB(20), nK(2);
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 6));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 1;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.1;
    bool to_prun = true;
    std::string rand_initializer("two");
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights, to_prun,
                        threshold,
                        rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    std::cout << "Threshold parameter for prunning: " << threshold << std::endl;
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}



TEST_F(GraphSynthTest, LargeCommunityDetectionNaive){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 500;// number of EM steps
    uint nRandInit = 10;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    std::string rand_initializer("two");
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights, to_prun,
                        threshold,
                        rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;

}

TEST_F(GraphSynthTest, LargeCommunityDetectionPrunnedOne){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 500;// number of EM steps
    uint nRandInit = 10;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.01f;
    bool to_prun = true;
    std::string rand_initializer("two");
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    std::cout << "Threshold parameter for prunning: " << threshold << std::endl;
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;

}

TEST_F(GraphSynthTest, LargeCommunityDetectionPrunnedTwo){
    TestGraphSynthetic g;
    // graph with 10000 vertices, 4500 in A community, 4500 in B
    // and 1000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(10000), nA(4500), nB(4500), nK(2);
    // 6 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 20));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 500;// number of EM steps
    uint nRandInit = 10;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.1f;
    bool to_prun = true;
    std::string rand_initializer("two");
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold,
                       rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    std::cout << "Threshold parameter for prunning: " << threshold << std::endl;
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    //show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;

}

#if 0

TEST_F(GraphSynthTest, HugeCommunityDetectionNaive){
    TestGraphSynthetic g;
    // graph with 50 vertices, 20 in A community, 20 in B
    // and 10 in A and B with equal chance.
    uint nV(100000), nA(45000), nB(45000), nK(2);
    std::cout << "Generating graph...\n";
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 10));
    std::cout << "Done.\n";
    //g.PrintGraph();


    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 10;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    std::string rand_initializer("two"); // name of concrete initializer
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                        threshold,
                        rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               false, //FIXME: no prunning
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}

TEST_F(GraphSynthTest, HugeCommunityDetectionPrunned){
    TestGraphSynthetic g;
    // graph with 100000 vertices, 45000 in A community, 45000 in B
    // and 10000 in A and B with equal chance.
    uint nV(100000), nA(45000), nB(45000), nK(2);
    std::cout << "Generating graph...\n";
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 10));
    std::cout << "Done.\n";
    //g.PrintGraph();


    // set parameters for algorithms
    uint nIterations = 100;// number of EM steps
    uint nRandInit = 10;// number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.1;
    std::string rand_initializer("two"); // name of concrete initializer
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                        threshold,
                        rand_initializer};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer(rand_initializer);
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               true, //FIXME: no prunning
                               *initializer_ptr);
    Detector.run();
    // show results
    //print2dVector(Detector.get_result());
    // show initial assignement
    //Detector.printInitialCommunities();
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}
#endif //end if(0)

}//end namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
