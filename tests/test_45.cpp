// test the output of the example
// with 45 papers

#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "community_config.h"
#include "parameters.h"
#include "utils.h"
#include "load_data.h"
#include "communitydetector.h"
#include "randominit.h"
#include "randominitfactory.h"
#include <map>
#include <boost/filesystem.hpp>

namespace{
 class TestPapers45 : public ::testing::Test {
 protected:

   virtual void SetUp() {
    // load date and initialize paramters
   }

   virtual void TearDown() {
     // Code here will be called immediately after each test (right
     // before the destructor).
   }

   // Objects declared here can be used by all tests in the test case for Foo.
 };


 /* TODO: add const correctly.*/
 bool test_45_result(std::vector<std::vector<float> > result){
 /*
  Test a particular case of the 45 papers check that the first 26 papers belong
  to one category and the next 19 to another.
  */
     bool test_passed = false;
     // sanity checks
     if (result.size() != 2) return false;
     if (result[0].size() != 918) return false;
     int first_cat(0), second_cat(0);
     for (uint i = 0; i < 26; ++i){
         first_cat += result[0][i] > result[1][i] ? +1 : -1;
     }
     for (uint i = 26; i < 45; ++i){
         second_cat += result[0][i] > result[1][i] ? +1 : -1;
     }
     // some magic numbers, which assume that algorithm was close enough
     // 6 mistakes for the first and second group: (26-6*2)=14, (19-6*2)=7
     if ((second_cat*first_cat < 0) && (std::abs(first_cat) > 14)
         && abs(second_cat) > 7){
        std::cout << "\e[32mThe test was passed\e[0m\n";
        test_passed = true;
    }
    else
        std::cout << "\e[31mThe test was failed\nMay be it's ok and you"
                  << " were just unlucky.\e[0m\n";
    //TODO: more meaningful message in case of failure
    std::cout << "First category: " << std::abs(first_cat)
              << "\nSecond category: " << std::abs(second_cat) << std::endl;

    return test_passed;
 }



 TEST_F(TestPapers45, CheckResult){
   // initialize paramters
   std::string test_dir = community_detection_TEST_DIR;
   std::string data_fname = test_dir + "/test_45_data.txt";
   std::string result_fname = test_dir + "/test_45_result.txt";
   std::string progress_fname = test_dir + "/test_45_progress.txt";
   // check if file exists
   boost::filesystem::path p(data_fname);
   boost::filesystem::file_status s = boost::filesystem::status(p);
   EXPECT_EQ(true, boost::filesystem::is_regular_file(s));
   //initialize the file names with data
    DataFileParameters file_params = {data_fname,
                                     result_fname,
                                     progress_fname};


   uint nV=918; // number of nodes
   uint nK=2; //number of communities
   uint nIterations = 100;// number of EM steps
   uint nRandInit = 10;// number of random initializations
   bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
   float threshold = 0.0f;
   bool to_prun = false;
   std::map<std::string, std::pair<uint, uint> > link_edges;
   std::map<std::string, float> link_weights;
   AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                                      ignore_weights, to_prun, threshold};
   // load data
   load_data(data_fname, link_weights, link_edges);
   algo_params.nE_ = link_edges.size(); // TODO: this should be set somwhere else
   RandomInit* initializer_ptr =
           RandomInitFactory::Instance().CreateRandomInitializer("RandomInitializer");

   // TODO: check if RandomInit == NULL;
   CommunityDetector C(algo_params, link_weights, link_edges,
                       *initializer_ptr);
   // run the algorithm
   C.run();
   Parameters params(algo_params, file_params);
   dumpResults(params, C.get_likelihoods(), C.get_result());
   // check the output
   EXPECT_EQ(true, test_45_result(C.get_result()));
   delete initializer_ptr;
}


}//end namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
