#include <gtest/gtest.h>
#include "testgraph.h"
#include <iostream>
#include <string>
#include "community_config.h"
#include "parameters.h"
#include "utils.h"
#include "communitydetector.h"
#include "randominit.h"
#include "randominitfactory.h"

namespace{

 class GraphSynthTest : public ::testing::Test {
 protected:

   virtual void SetUp() {
     // Code here will be called immediately after the constructor (right
     // before each test).
   }

   virtual void TearDown() {
     // Code here will be called immediately after each test (right
     // before the destructor).
   }

   // Objects declared here can be used by all tests in the test case for Foo.
 };


TEST_F(GraphSynthTest, LargeCommunityDetection){
    TestGraphSynthetic g;
    // graph with 100000 vertices, 40000 in A community, 40000 in B
    // and 20000 in A and B with equal chance.
    // This is similar to experiment in the original paper.
    uint nV(100000), nA(40000), nB(40000), nK(2);
    // 60 links per node
    EXPECT_EQ(true, g.GenerateGraph(nV, nA, nB, 60));
    //g.PrintGraph();

    // set parameters for algorithms
    uint nIterations = 500; // number of EM steps
    uint nRandInit = 16; // number of random initializations
    bool ignore_weights = 1; // 1 to ignore weights of links, 0 to keep them
    const float threshold = 0.0;
    bool to_prun = false;
    AlgorithmParameters algo_params = {nK, nV, 0, nIterations, nRandInit,
                       ignore_weights,
                       to_prun,
                       threshold};

    algo_params.nE_ = g.get_link_edges().size(); // TODO: remove it
    std::cout << "Number of vertices: " <<  nV
              << "\nNumber of edges: " << algo_params.nE_ << std::endl;

    // chose random initialization scheme for edges
    RandomInit* initializer_ptr =
            RandomInitFactory::Instance().
            CreateRandomInitializer("RandomInitializer");
    // run algorithm
    CommunityDetector Detector(algo_params, g.get_link_weights(),
                               g.get_link_edges(),
                               *initializer_ptr);
    Detector.run();
    // show results
    // print2dVector(Detector.get_result());
    // check the results
    double error = g.TestResult(Detector.get_result());
    std::cout << "Average error: " << error << std::endl;
    EXPECT_LE(error, 0.2);
    delete initializer_ptr;
}


}//end namespace

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
