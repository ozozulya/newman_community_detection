
This is the code that implements Newman's algortihm
of detecting overlapping communities in the networks
as introduced in http://sciencewise.info/articles/1104.3590

The first version was written by Zhao Yang and is the initial 
git code - version 0. The further modifications are version controlled.


CMAKE COMPILATION

1. Install GETOPT library. 
One has to compile getoptpp static library which is in the
folder getoptpp/getoptpp_src. 
The resulting file getoptlib.a should be moved into the
the directory getoptpp/lib.

2. Install GTEST library (under development).
gtest is the framework used for testing.
Therefore one has either to install the libray or
clone it to the root of the project and it will be 
automatically compiled (because gtest supports
cmake framework).

2. To compile one can use cmake command.
First create directory build in the root of the project:
   mkdir build
   cd build
then issue: 
   cmake ../
then
    make VERBOSE=1
The executables will be written to "bin" directory in the root of the projcet.



UNIX COMPILATION

Unix Makefile_manual is a the moment broken, and 
has low priority to be fixed.
Equivalently, one could  use the file Makefile_manual,
to compile on the unix-like systems:
    make -f Makefile_manual
